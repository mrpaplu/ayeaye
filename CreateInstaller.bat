REM @echo OFF
call "%VS110COMNTOOLS%..\..\VC\vcvarsall.bat"
set pathMSBuild="%FrameworkDir%\%FrameworkVersion%"
echo "Building project in Release Mode"
%pathMSBuild%\MSBuild /t:Build /p:Configuration=Release;PlatformToolset=v110 ayeaye\ayeaye.vcxproj  

echo "Copying files for the installer"
rmdir /S /Q Installer
mkdir Installer
cd Installer
mkdir bin
mkdir GameData
copy /Y ..\ayeaye\Release\*.exe bin\
copy /Y ..\ayeaye\Release\*.dll bin\
xcopy /E /Y ..\GameData\* GameData\
del GameData\*.ayesave
del GameData\*.ayescore

echo "Creating installer"

del ..\AyeAye.exe

set rar_path=C:\Program Files\WinRAR
"%rar_path%\winrar" a -sfx -z"..\ayeayeInstall.conf" "..\AyeAye.exe" bin GameData
cd ..
"%rar_path%\winrar" s -iimginstallerlogo.bmp -iiconpirate.ico AyeAye.exe 

rmdir /S /Q Installer
echo DONE
