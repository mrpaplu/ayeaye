# AyeAye #

AyeAye is a pirate themed 2D scroller we built in 10 weeks using only C++ and Win32. 

Download: [AyeAye.exe](https://bitbucket.org/mrpaplu/ayeaye/downloads/AyeAye.exe)

![pirate.png](https://bitbucket.org/repo/9AKyKa/images/2696309879-pirate.png)

## Drink rum! ##

Drink rum and score points!

![rum.png](https://bitbucket.org/repo/9AKyKa/images/2411289771-rum.png)

## Avoid danger! ##

Aye has a slightly different trip on his shrooms. Yes, he dies.

![shroom.png](https://bitbucket.org/repo/9AKyKa/images/4007457204-shroom.png)

## Battle the enemy ##

Blackbeard is your sworn enemy. Don't like him taking your hearts? Jump on his head and he'll regret ever killing you.

![blackbeard.png](https://bitbucket.org/repo/9AKyKa/images/755800787-blackbeard.png)

# Authors #

* Justin Peuijn
* Wouter Brunekreeft
* Carlos Bruckner
* Daan Flobbe
* Arjan Speiard
* Kay van Bree