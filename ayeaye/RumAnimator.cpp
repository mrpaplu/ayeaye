#include "stdafx.h"
#include "RumAnimator.h"

using namespace Controllers;

RumAnimator::RumAnimator(std::vector<Models::Sprite*> sprites, Models::GameObject* ourObject) 
	: Animator(sprites, ourObject), _curOffset(0), _timeout(0)
{
}

RumAnimator::~RumAnimator(void)
{
}

void RumAnimator::UpdateSprite(float delta)
{
	if (!_ourObject)
	{
		return;
	}

	_timeout += delta;

	if (_timeout >= 0.15)
	{
		_timeout = 0;
		_curOffset++;
		if (_curOffset >= 4)
			_curOffset = 0;
	}
	
	_currentSprite = _sprites[_curOffset];
}