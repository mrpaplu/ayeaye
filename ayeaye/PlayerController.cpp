#include "stdafx.h"
#include "Player.h"
#include "PlayerController.h"
#include <iostream>


using namespace Controllers;

PlayerController::~PlayerController()
{
}

void PlayerController::SetSoundController(SoundController* sc)
{
	_soundController = sc;
}

void PlayerController::Update(float delta) //2.97, 0.12
{
	if (!_player)
	{
		return;
	}

	// physics
	if (GetPlayer()->IsFalling())
	{
		FallDown(delta);
		Point<float> v = _player->GetVelocity();
		if (v.y > 200)
		{
			v.y = 200;
			_player->SetVelocity(v);
		}
	}

	GetPlayer()->SetIsFalling(true);

	Point<float> playerPosition = _player->GetPosition();
	playerPosition.x += _player->GetVelocity().x * delta;
	playerPosition.y += _player->GetVelocity().y * delta;
	_player->SetPosition(playerPosition);
} 

void PlayerController::OnCollision(Models::GameObject* other, Models::CollisionDirection d)
{
	if(other->GetType() == "Jungle-plant" || 
		other->GetType() == "Jungle-sign-left" || 
		other->GetType() == "Jungle-sign-right")
	{
		// Do nothing
		return;
	}
	if (other->GetType() == "Rum" || other->GetType() == "Vodka") 
	{
		_soundController->GetView()->Play(&_rumSound);
		other->MarkForDeletion(true);
		_player->AddScore(100);
	}
	else if(other->GetType() == "Heart")
	{
		if (_player->GetLives() < 3)
		{
			_soundController->GetView()->Play(&_heartSound);
			other->MarkForDeletion(true);
			_player->SetLives(_player->GetLives() + 1);
		}
	}
	else if(other->GetType() == "bomb" || other->GetType() == "Mushroom" || other->GetType() == "Water") 
	{
		_soundController->GetView()->Play(_player->GetDieSound());
		_player->Respawn();
	}
	else if(other->GetType() == "blackbeard")
	{
		if (d == Models::CollisionDirection::Top) 
		{			
			other->MarkForDeletion(true);
			_player->AddScore(500);
		}
		else
		{
			_soundController->GetView()->Play(_player->GetDieSound());
			_player->Respawn();
		}
	}
	else if(other->GetType() == "Spacefade")
	{
		other->MarkForDeletion(true);
	}
	else if(other->GetType() == "Portal")
	{
		if(GetPlayer()->GetPosX() >= other->GetPosX() && 
			GetPlayer()->GetPosX() + GetPlayer()->GetWidth() <= other->GetPosX() + other->GetWidth() &&
			GetPlayer()->GetPosY() >= other->GetPosY() &&
			GetPlayer()->GetPosY() + GetPlayer()->GetHeight() <= other->GetPosY() + other->GetHeight())
		{
			_player->Win();
		}
	}
	else if (other->GetType() == "Small-house" || other->GetType() == "Big-house"){
		return;
	}
	else
	{
		switch (d)
		{
		case Models::CollisionDirection::Top:
			GetPlayer()->SetPosY(other->GetPosY() - GetPlayer()->GetHeight());
			GetPlayer()->SetIsFalling(false);
			break;
		case Models::CollisionDirection::Bottom:
		{
			GetPlayer()->SetPosY(other->GetPosY() + other->GetHeight());
			Point<float> v = { GetPlayer()->GetVelocity().x, 0 };
			GetPlayer()->SetVelocity(v);
		}
			break;
		case Models::CollisionDirection::Left:
			GetPlayer()->SetPosX(other->GetPosX() - GetPlayer()->GetWidth());
			break;
		case Models::CollisionDirection::Right:
			GetPlayer()->SetPosX(other->GetPosX() + other->GetWidth());
			break;
		default:
			break;
		}
	}
}

void PlayerController::OnOutOfLevel(Models::CollisionDirection dir, bool outOfLevel, RECT levelBounds)
{
	if (dir != Models::CollisionDirection::Top){
		if (outOfLevel)
		{
			GetPlayer()->Respawn();
		}
		else if (dir == Models::CollisionDirection::Left)
		{
			GetPlayer()->SetPosX(levelBounds.left);
		}
		else if (dir == Models::CollisionDirection::Right)
		{
			GetPlayer()->SetPosX(levelBounds.right - GetPlayer()->GetWidth());
		}
	}
}

void PlayerController::InputLeft()
{
	if (!_player)
		return;
	if(!GetPlayer()->GetKeyboard()->left)
	{
		MoveLeft();
	}
}

void PlayerController::MoveLeft()
{
	Point<float>p = { -(_player->GetDefaultVelocity().x), 0 };
	_player->AlterMovement(p);
	_player->GetKeyboard()->left = true;
}

void PlayerController::InputRight()
{
	if (!_player)
		return;
	if(!GetPlayer()->GetKeyboard()->right)
	{
		MoveRight();
	}
}

void PlayerController::MoveRight()
{
	Point<float>p = {(_player->GetDefaultVelocity().x), 0 };
	_player->AlterMovement(p);
	_player->GetKeyboard()->right = true;
}

void PlayerController::InputTop()
{
	if (!_player)
		return;
	if(!_player->GetKeyboard()->up)
	{
		if (!_player->IsFalling())
		{
			Jump();
			_player->GetKeyboard()->up = true;
		}
	}
}

void PlayerController::InputBottom()
{
	if (!_player)
		return;
	if(!_player->GetKeyboard()->down)
	{
		Point<float>p;
		p.y = (_player->GetDefaultVelocity().y);
		p.x = 0;
		_player->AlterMovement(p);
		_player->GetKeyboard()->down = true;
	}
}

void PlayerController::StopLeft()
{
	if (!_player)
		return;
	_player->GetKeyboard()->left = false;
	_player->GetMovement()->StopX();

	if (_player->GetKeyboard()->right)
	{
		MoveRight();
	}
}

void PlayerController::StopRight()
{
	if (!_player)
		return;
	_player->GetKeyboard()->right = false;
	_player->GetMovement()->StopX();

	if (_player->GetKeyboard()->left)
	{
		MoveLeft();
	}
}

void PlayerController::StopY()
{
	if (!_player)
		return;
	_player->GetKeyboard()->up = false;
	_player->GetKeyboard()->down = false;
	_player->GetMovement()->StopY();
}

void PlayerController::SetPlayer(Models::Player* p)
{
	_player = p;
}

Models::Player* PlayerController::GetPlayer()
{
	return _player;
}

void PlayerController::FallDown(float delta)
{
	if (!_player && !_player->IsFalling())
		return;
	Point<float> p = { 0, 400 * delta };
	_player->GetMovement()->AlterMovement(p);
}

void PlayerController::Jump()
{
	if (!_player)
		return;
	_soundController->GetView()->Play(_player->GetJumpSound());
	Point<float> p = { _player->GetVelocity().x, -270 };
	_player->SetVelocity(p);
}

Models::Sound* PlayerController::GetRumSound()
{
	return &_rumSound;
}

Models::Sound* PlayerController::GetHeartSound()
{
	return &_heartSound;
}