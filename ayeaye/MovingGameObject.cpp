#include "stdafx.h"
#include "MovingGameObject.h"

using namespace Models;

MovingGameObject::MovingGameObject(void)
{
}

MovingGameObject::MovingGameObject(Point<float> p) 
{
	_position = p;
	_isFalling = false;
}

MovingGameObject::~MovingGameObject(void)
{
}

void MovingGameObject::SetVelocity(Point<float> p)
{
	_movement.SetVelocity(p);
}

Point<float> MovingGameObject::GetVelocity()
{
	return _movement.GetVelocity();
}

Point<float> MovingGameObject::GetDefaultVelocity() const
{
	return _defaultVelocity;
}						

bool MovingGameObject::IsFalling()
{
	return _isFalling;
}

void MovingGameObject::SetIsFalling(bool isFalling)
{
	_isFalling = isFalling;
}

bool MovingGameObject::IsMoveable()
{
	return true;
}

void MovingGameObject::AlterMovement(Point<float>& p)
{
	_movement.AlterMovement(p);
}

Movement* MovingGameObject::GetMovement()	
{
	return &_movement;
}

