#include "stdafx.h"
#include "SoundView.h"

using namespace Views;

SoundView::SoundView(void)
{
}


SoundView::~SoundView(void)
{
}

bool SoundView::Play(Models::Sound* sound)
{
	if (!sound->Exists()) return false;

	HRESULT result;
 
	IDirectSoundBuffer8** _secondaryBuffer = sound->GetSecondaryBuffer();
 
	// Set position at the beginning of the sound buffer.
	result = (*_secondaryBuffer)->SetCurrentPosition(0);
	if(FAILED(result))
	{
		return false;
	}
 
	// Set volume of the buffer to 100%.
	result = (*_secondaryBuffer)->SetVolume(DSBVOLUME_MAX);
	if(FAILED(result))
	{
		return false;
	}
 
	// Play the contents of the secondary sound buffer.
	result = (*_secondaryBuffer)->Play(0, 0, 0);
	if(FAILED(result))
	{
		return false;
	}
	sound->SetIsPlaying(true);
	return true;
}

bool SoundView::PlayLoop(Models::Sound* sound)
{
	if (!sound->Exists()) return false;
	HRESULT result;
 
	IDirectSoundBuffer8** _secondaryBuffer = sound->GetSecondaryBuffer();
 
	// Set position at the beginning of the sound buffer.
	result = (*_secondaryBuffer)->SetCurrentPosition(0);
	if(FAILED(result))
	{
		return false;
	}
 
	// Set volume of the buffer to 100%.
	result = (*_secondaryBuffer)->SetVolume(DSBVOLUME_MAX);
	if(FAILED(result))
	{
		return false;
	}
 
	// Play the contents of the secondary sound buffer.
	result = (*_secondaryBuffer)->Play(0, 0, DSBPLAY_LOOPING);
	if(FAILED(result))
	{
		return false;
	}
	sound->SetIsPlaying(true);
	return true;
}

void SoundView::Stop(Models::Sound* sound)
{
	if(sound->Exists())
	{
		IDirectSoundBuffer8** _secondaryBuffer = sound->GetSecondaryBuffer();
		(*_secondaryBuffer)->Stop();
		sound->SetIsPlaying(0);
	}
}