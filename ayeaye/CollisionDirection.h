#pragma once

namespace Models
{
	enum class CollisionDirection {
		Top,
		Bottom,
		Left,
		Right,
		None,
	};
}