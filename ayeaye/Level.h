#pragma once

#include <vector>
#include "GameObject.h"
#include "Background.h"
#include "Sound.h"

namespace Models
{
	class Player;

	class Level
	{
	public:								 
		typedef std::vector<GameObject*>::iterator gameobjects_iterator;
		typedef std::vector<GameObject*>::const_iterator const_gameobjects_iterator;
		Level();
		~Level();
		int GetWidth() const;
		int GetHeight() const;
		void SetWidth(int);
		void SetHeight(int);
		const_gameobjects_iterator GameObjectsBegin() const;
		const_gameobjects_iterator GameObjectsEnd() const;
		gameobjects_iterator GameObjectsBegin();
		gameobjects_iterator GameObjectsEnd();
		void AddGameObject(Models::GameObject*);		
		void SetPlayer(Player*);
		void SetBackground(Background*);
		Player* GetPlayer();
		Background* GetBackground();
		void DestroyLevel();
		void DeleteDeletedObjects();
		void SetLevelName(std::string);
		std::string GetLevelName();
		Sound* GetMusic();
	private:
		int _width;
		int _height;
		std::vector<GameObject*> _gameObjects;
		Player* _player;
		Background* _background;
		std::string _levelName;
		Sound _bgMusic;
	};
}
