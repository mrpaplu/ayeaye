#pragma once

#include "Point.h"
#include "Animator.h"
#include <string>
#include "GameObjectProperties.h"

namespace Controllers
{
	class animator;
	class GameObjectController;
}

namespace Models
{
	class Sprite;

	class GameObject
	{
	public:
		GameObject();
		GameObject(Point<float>);
		~GameObject();
		int GetWidth() const;
		int GetHeight() const;
		float GetPosX() const;
		float GetPosY() const;
		std::string GetType() const;
		void SetWidth(int);
		void SetHeight(int);
		void SetPosX(float);
		void SetPosY(float);
		void SetType(std::string);
		const Point<float> GameObject::GetPosition() const;
		virtual Point<float> GetVelocity();
		virtual void SetVelocity(Point<float> p);
		virtual Point<float> GetDefaultVelocity() const;
		void SetPosition(Point<float>);
		virtual bool GameObject::IsFalling();
		virtual void GameObject::SetIsFalling(bool);
		void SetSprite(Models::Sprite* sprite);
		Sprite* GetSprite() const;
		void SetController(Controllers::GameObjectController*);
		Controllers::GameObjectController* GetController() const;
		void SetCollisionBox();
		virtual bool IsMoveable();
		static GameObject* GameObject::MakeGameObject(const Loaders::GameObjectProperties&);
		void MarkForDeletion(bool);
		bool IsMarkedForDeletion();
	private:
		int _height;
		int _width;
		std::string _type;
		Controllers::GameObjectController* _controller;
	protected:
		Sprite* _sprite;
		Point<float> _position;
		bool _markedForDeletion;
	};
}