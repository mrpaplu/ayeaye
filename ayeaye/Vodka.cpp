#include "stdafx.h"
#include "Vodka.h"

using namespace Models;

Vodka::Vodka()
{
}


Vodka::~Vodka()
{
}

Vodka* Vodka::MakeGameObject(const Loaders::GameObjectProperties& props)
{
	Models::Vodka * go = new Models::Vodka();
	Controllers::Animator * animator = new Controllers::RumAnimator(props.sprites, go);
	go->SetWidth(props.width);
	go->SetHeight(props.height);
	go->SetPosX(props.posx);
	go->SetPosY(props.posy);
	go->SetType(props.type);
	go->SetSprite(animator);
	return go;
}
