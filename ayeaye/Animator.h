#pragma once

#include <vector>
#include <Windows.h>
#include "Sprite.h"

namespace Models
{
	class Sprite;
	class GameObject;
}

namespace Controllers
{
	class Animator : public Models::Sprite
	{
	public:
		Animator(std::vector<Models::Sprite*> sprites, Models::GameObject* ourObject);
		virtual void UpdateSprite(float delta) = 0;
		int GetWidth() const;
		int GetHeight() const;
		virtual void Render(HDC hdc, int xDest, int yDest) const;
		~Animator();
	protected:
		std::vector<Models::Sprite*> _sprites;
		Models::GameObject* _ourObject;
		Models::Sprite * _currentSprite;
	};
}