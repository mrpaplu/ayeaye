#include "stdafx.h"

#include "Blackbeard.h"
#include "BlackbeardController.h"

using namespace Models;

Blackbeard::Blackbeard(Point<float> p)
	: MovingEnemy(p)
{
	Point<float> velocity = { 180, 200 };
	SetVelocity(velocity);
	SetPosition(p);
}

Blackbeard::~Blackbeard(void)
{
}

Blackbeard* Blackbeard::MakeGameObject(const Loaders::GameObjectProperties& props)
{
	Point<float> point = { props.posx, props.posy };
	Models::Blackbeard * go = new Models::Blackbeard(point);
	Controllers::Animator * animator = new Controllers::BlackbeardAnimator(props.sprites, go);
	Controllers::BlackbeardController * ctrl = new Controllers::BlackbeardController();
	ctrl->SetModel(go);
	go->SetController(ctrl);
	go->SetWidth(props.width);
	go->SetHeight(props.height);
	go->SetPosX(props.posx);
	go->SetPosY(props.posy);
	go->SetType(props.type);
	go->SetSprite(animator);
	return go;
}
