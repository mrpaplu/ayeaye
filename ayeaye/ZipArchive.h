#pragma once

#include <vector>
#include <streambuf>
#include <exception>
#include <zip.h>

namespace Loaders
{
	class ZipFileBuffer : public std::streambuf
	{
	public:
		~ZipFileBuffer();
	private:
		ZipFileBuffer(struct zip*, struct zip_stat*);
		int_type underflow();
		struct zip_file* _file;
		const static unsigned int _maxBufferSize = 1024;
		std::vector<char> _buffer;
		friend class ZipFile;
	};

	class ZipFile
	{
	public:
		~ZipFile();
		std::string Name();
		int Index();
		std::streambuf* CreateInputStream();
	private:
		ZipFile(struct zip* archive, struct zip_stat*);
		struct zip_stat* _info;
		struct zip* _myArchive;
		std::vector<std::streambuf*> _streambufs;
		friend class ZipArchive;
	};

	class ZipArchive
	{
	public:
		typedef std::vector<ZipFile*>::const_iterator const_iterator;
		ZipArchive(std::string);
		~ZipArchive();
		const_iterator begin() const;
		const_iterator end() const;
		std::streambuf* Open(const std::string& fname);
		bool FileExists(const std::string& fname);
	private:
		struct zip* _archive;
		std::vector<ZipFile*> _files;
	};

	class ZipException : public std::exception
	{
	public:
		ZipException(int ze);
		ZipException(std::string what);
		virtual const char* what() const throw();
	private:
		std::string _message;
	};
}