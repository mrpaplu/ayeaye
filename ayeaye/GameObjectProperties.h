#pragma once

#include <string>

namespace Models {
	class Sprite;
}

namespace Loaders {
	struct GameObjectProperties
	{
		int width;
		int height;
		int posy;
		int posx;
		std::string type;
		std::vector<Models::Sprite*> sprites;
	};
}