#pragma once

#include "SoundView.h"

namespace Controllers
{
	class SoundController
	{
	public:
		IDirectSound8* GetDirectSound();
		IDirectSoundBuffer* GetPrimaryBuffer();
		Views::SoundView* GetView();
		SoundController(HWND);
		SoundController();
		~SoundController(void);
	private:
		IDirectSound8* _directSound;
		IDirectSoundBuffer* _primaryBuffer;
		void ShutdownDirectSound();
		bool InitializeDirectSound(HWND);
		Views::SoundView _soundView;
	};
}