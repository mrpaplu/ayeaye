#include "stdafx.h"
#include "GameObject.h"
#include "Sprite.h"
#include <algorithm>

using namespace Models;

GameObject::GameObject()
: _height(0), _width(0)
{	
	_markedForDeletion = false;
}

GameObject::~GameObject()
{
}

GameObject::GameObject(Point<float> p)
: _height(0), _width(0)
{
	_markedForDeletion = false;
	_position = p;
}

int GameObject::GetHeight() const
{
	return _height;
}

int GameObject::GetWidth() const
{
	return _width;
}

float GameObject::GetPosX() const
{
	return _position.x;
}

void GameObject::SetType(std::string type)
{
	_type = type;
}
std::string GameObject::GetType() const
{
	return _type;
}

float GameObject::GetPosY() const	 
{
	return _position.y;
}

void GameObject::SetWidth(int w) 
{
	_width = w;
}

void GameObject::SetHeight(int h)	
{
	_height = h;
}

void GameObject::SetPosX(float x)	
{
	_position.x = x;
}

void GameObject::SetPosY(float y) 
{
	_position.y = y;
}

const Point<float> GameObject::GetPosition() const 
{
	return _position;
}

  
void GameObject::SetPosition(Point<float> point)
{
	_position = point;
}		   
 
void GameObject::SetCollisionBox()
{
	SetWidth(GetSprite()->GetWidth());
	SetHeight(GetSprite()->GetHeight());
}

void GameObject::SetSprite(Models::Sprite* sprite)
{
	_sprite = sprite;
	SetHeight(sprite->GetHeight());
}

Models::Sprite* GameObject::GetSprite() const
{
	return _sprite;
}

void GameObject::SetController(Controllers::GameObjectController* c)
{
	_controller = c;
}

Controllers::GameObjectController* GameObject::GetController() const
{
	return _controller;
}

GameObject* GameObject::MakeGameObject(const Loaders::GameObjectProperties& props)
{
	Models::GameObject * go = new Models::GameObject();
	go->SetWidth(props.width);
	go->SetHeight(props.height);
	go->SetPosX(props.posx);
	go->SetPosY(props.posy);
	go->SetType(props.type);
	go->SetSprite(props.sprites[0]);
	return go;
}

Point<float> GameObject::GetVelocity()
{
	const Point<float> v = { 0, 0 };
	return v;
}

Point<float> GameObject::GetDefaultVelocity() const
{
	const Point<float> v = { 0, 0 };
	return v;
}

void GameObject::SetVelocity(Point<float> p)
{

}

bool GameObject::IsMoveable()
{
	return false;
}

bool GameObject::IsFalling()
{
	return false;
}

void GameObject::SetIsFalling(bool) {}

void GameObject::MarkForDeletion(bool mark)
{
	_markedForDeletion = mark;
}

bool GameObject::IsMarkedForDeletion()
{
	return _markedForDeletion;
}