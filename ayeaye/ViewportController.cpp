#include "stdafx.h"
#include "ViewportController.h"

using namespace Controllers;

ViewportController::ViewportController(void)
{
}


ViewportController::~ViewportController(void)
{
}

ViewportController::ViewportController(Point<int> size)
{
	_viewport = Models::Viewport(size);
}

ViewportController::ViewportController(Point<int> pos, Point<int> size)
{
	_viewport = Models::Viewport(pos, size);
}

Models::Viewport* ViewportController::GetViewport()
{
	return &_viewport;
}

void ViewportController::Update(Point<float> playerPosition, int maxY, int maxX)
{
	Point<int> newScreenPosition = {
		playerPosition.x - (_viewport.GetSize().x) / 2,
		playerPosition.y - (_viewport.GetSize().y) / 2 };
	if (newScreenPosition.x < 0) 
	{
		newScreenPosition.x = 0;
	}
	else if (newScreenPosition.x + _viewport.GetSize().x > maxX) 
	{
		newScreenPosition.x = maxX - _viewport.GetSize().x;
	}

	if (newScreenPosition.y < 0) 
	{
		newScreenPosition.y = 0;
	}
	if (newScreenPosition.y + _viewport.GetSize().y > maxY) 
	{
		newScreenPosition.y = maxY - _viewport.GetSize().y;
	}
	

	_viewport.SetPosition(newScreenPosition);
}

Point<float> ViewportController::TranslateCoordinates(Point<float> ptMap)
{
	Point<float> ptScreen;
	ptScreen.x = ptMap.x - _viewport.GetPosition().x;
	ptScreen.y = ptMap.y - _viewport.GetPosition().y;
	return ptScreen;
}

Point<int> ViewportController::TranslateCoordinates(Point<int> ptMap)
{
	Point<int> ptScreen;
	ptScreen.x = ptMap.x - _viewport.GetPosition().x;
	ptScreen.y = ptMap.y - _viewport.GetPosition().y;
	return ptScreen;
}

bool ViewportController::IsVisible(const Point<float> position, int width, int height)
{
	Point<int> viewportSize = _viewport.GetSize();
	Point<float> translated = TranslateCoordinates(position);
	return !(
		translated.x > viewportSize.x ||
		translated.x + width < 0 ||
		translated.y > viewportSize.y ||
		translated.y + height < 0);
}

int* ViewportController::TranslateBackground(int backgroundNumber, Point<int>& position, int& numImages, int width, int height)
{
	numImages = 0;
	position.x = position.x - _viewport.GetPosition().x / 3 * (backgroundNumber + 1);
	
	int difference_y; 

	if (position.y == 0)
		difference_y = 1;
	else
		difference_y = position.y + height - Bottom(); 

	position.y = position.y - (_viewport.GetPosition().y - difference_y) / 3 * (backgroundNumber + 1);	
	
	while (!(position.x <= 0 && position.x + width >= 0))
	{
		if(position.x > 0) 
			position.x -= width;
		else
			position.x += width;
	}
	numImages = (_viewport.GetSize().x / (width)) + 2;

	if(numImages <= 0) 
	{
		numImages = 1;
	}
	int* replicatePositions = new int[numImages];
	replicatePositions[0] = position.x;
	for (int i = 1; i < numImages; i++)
	{
		replicatePositions[i] = replicatePositions[i - 1] + width;
	}
	return replicatePositions;
}

void ViewportController::UpdateWindowSize(Point<int> windowSize)
{
	_viewport.SetSize(windowSize);
}

int ViewportController::Top()
{
	return _viewport.GetPosition().y;
}

int ViewportController::Bottom()
{
	return _viewport.GetPosition().y + _viewport.GetSize().y;
}

int ViewportController::Left()
{
	return _viewport.GetPosition().x;
}

int ViewportController::Right()
{
	return _viewport.GetPosition().x + _viewport.GetSize().x;
}