#pragma once

#include <string>
#include <vector>
#include "Sound.h"

namespace Models 
{
	class Menu
	{
	public:
		Menu(void);
		~Menu(void);
		bool IsActive();
		void SetActive(bool);
		void SetContent(bool);
		std::vector<std::string>* GetContent();
		std::vector<std::string>* GetMenuItems();
		typedef std::vector<std::string>::const_iterator const_menuitem_iterator;
		typedef std::vector<std::string>::const_iterator const_content_iterator;
		const_menuitem_iterator MenuItemsBegin() const;
		const_menuitem_iterator MenuItemsEnd() const;
		const_content_iterator ContentBegin() const;
		const_content_iterator ContentEnd() const;
		int GetSelectedIndex();
		void SelectPrevious();
		void SelectNext();
		bool HasBackground();
		bool HasContent();
		int GetHeight();
		int GetWidth();
		HBITMAP GetBackgroundImage();
		bool IsToggleable();
		virtual void SetMenuItems() = 0;
	protected:
		bool _hasContent;
		bool _isActive;
		bool _isToggleable;
		std::vector<std::string> _content;
		std::vector<std::string> _items;
		int _selectedItem;
		bool _hasBackground;
		int _width;
		int _height;
		HBITMAP _backgroundImage;
	};
}