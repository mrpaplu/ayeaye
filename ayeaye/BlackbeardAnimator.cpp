#include "stdafx.h"
#include "BlackbeardAnimator.h"
#include "MovingGameObject.h"

using namespace Controllers;

BlackbeardAnimator::BlackbeardAnimator(std::vector<Models::Sprite*> sprites, Models::GameObject* ourObject) 
	: Animator(sprites, ourObject), _curOffset(0), _timeout(0)
{
}


BlackbeardAnimator::~BlackbeardAnimator(void)
{
}

void BlackbeardAnimator::UpdateSprite(float delta)
{
	if (!_ourObject)
	{
		return;
	}

	_timeout += delta;
	Point<float> velocity = _ourObject->GetVelocity();

	if (_timeout >= 0.15)
	{
		_timeout = 0;
		_curOffset++;
		if (_curOffset >= 3)
			_curOffset = 0;
	}
	else if (velocity.x == 0)
	{				 
		_curOffset = 1;
	}

	if (velocity.x < 0)
	{
		_isLeft = true;
	}
	else if (velocity.x > 0)
	{
		_isLeft = false;
	}

	if (_isLeft)
	{
		_currentSprite = _sprites[_curOffset];
	}
	else
	{
		_currentSprite = _sprites[3 + _curOffset];
	}
}