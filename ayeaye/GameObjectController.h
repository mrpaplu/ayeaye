#pragma once

#include <Windows.h>
#include "GameObject.h"
#include "CollisionDirection.h"

namespace Controllers {
	class GameObjectController
	{
	public:
		GameObjectController();
		~GameObjectController();
		virtual void Update(float delta) = 0;
		virtual void OnCollision(Models::GameObject* other, Models::CollisionDirection d) = 0;
		virtual void OnOutOfLevel(Models::CollisionDirection dir, bool outOfLevel, RECT levelBounds) = 0;
	};
}