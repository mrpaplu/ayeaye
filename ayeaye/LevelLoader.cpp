#include "stdafx.h"
		   			
#include <string>
#include <iostream>
#include <zip.h>
#include "LevelLoader.h"
#include "LevelLoaderError.h"
#include "SpriteMapLoader.h"
#include "Utils.h"

using namespace Loaders;

LevelLoader::LevelLoader()
{
}

LevelLoader::~LevelLoader()
{
}

Models::Level LevelLoader::Load(HWND window, std::istream& input, Models::Bitmap * bmp)
{
	Models::Level level;
	std::string line;
	Utils::getline(input, line);
	line = Utils::Trim(line);

	std::vector<Models::Sprite*> spriteMap;

	if (line != "BEGIN:LEVEL")
		throw LevelLoad_error();

	while (Utils::getline(input, line))
	{
		if (line.empty())
			continue;
		std::string key = Utils::Trim(line.substr(0, line.find(':')));
		std::string value = Utils::Trim(line.substr(line.find(':') + 1));
		if (key == "WIDTH")
			level.SetWidth(atoi(value.c_str()));
		else if (key == "HEIGHT")
			level.SetHeight(atoi(value.c_str()));
		else if (key == "BEGIN" && value == "GAMEOBJECT")
		{
			Models::GameObject * go = _gameObjectLoader.Load(window, input, spriteMap);
			if (_stricmp(go->GetType().c_str(), "PLAYER") == 0)
				level.SetPlayer((Models::Player*)go);
			level.AddGameObject(go);
		}
		else if (key == "BEGIN" && value == "SPRITEMAP")
			spriteMap = SpriteMapLoader::LoadSpriteMap(input, bmp);
		else if (key == "BEGIN" && value == "BACKGROUND")
		{
			Models::Background* bg = BackgroundLoader::LoadBackground(window, input);
			level.SetBackground(bg);
		}
		else if (key == "END" && value == "BACKGROUND")
			{}//ignore
		else if (key == "BEGIN" && value == "LAYER")
			{}//ignore
		else if (key == "END" && value == "LAYER")
			{}//ignore
		else if (key == "END" && value == "LEVEL")
		{
			return std::move(level);
		}
		else if (key == "GRAVITY") {} //ignore
		else
			throw LevelLoad_error();
	}
	throw LevelLoad_error();
}