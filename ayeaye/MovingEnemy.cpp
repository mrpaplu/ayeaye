#include "stdafx.h"
#include "MovingEnemy.h"
#include "GameController.h"

using namespace Models;

MovingEnemy::MovingEnemy(void)
{
}


MovingEnemy::~MovingEnemy(void)
{
}

MovingEnemy::MovingEnemy(Point<float> point)
	: MovingGameObject(point)
{
	_startPosition = point;
	_range = 100;
}

bool MovingEnemy::CanMoveLeft()
{
	return _startPosition.x - _range < _position.x;
}

bool MovingEnemy::CanMoveRight()
{
	return _startPosition.x + _range > _position.x;
}