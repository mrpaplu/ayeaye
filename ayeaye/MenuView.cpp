#include "stdafx.h"
#include "MenuView.h"

using namespace Views;

MenuView::MenuView(void)
{
}

MenuView::MenuView(Models::Menu* menu)
{
	_menu = menu;
}

MenuView::~MenuView(void)
{
}

void MenuView::Paint(HWND hWnd, PAINTSTRUCT* paintstruct)
{
	RECT desktop;
    // Get the size of screen to the variable desktop
    GetClientRect(hWnd, &desktop);

	int centerX = desktop.right / 2;
	int centerY = desktop.bottom / 2;

	DrawBackground(paintstruct, centerX, centerY);

	// Set stuff for writing text
	_fontHeight = 32;
	_leftMargin = _fontHeight;
	_currentY = centerY - _menu->GetHeight() / 2;
	_textX = centerX - _menu->GetWidth() / 2;
	AddFontResource("..\\GameData\\PiratesBay.ttf");
	HFONT font = CreateFont(_fontHeight, 0, 0, 0, 0, FALSE, FALSE, FALSE, 
		DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, 
		DEFAULT_QUALITY, FF_DONTCARE, TEXT("PiratesBay"));
	SetBkMode(paintstruct->hdc, TRANSPARENT);
	SetTextColor(paintstruct->hdc, RGB(28,17,11));
	SelectObject(paintstruct->hdc, font);

	// Write menu content and items
	if (_menu->HasContent())
	{
		DrawContent(paintstruct, centerX, centerY);
	}
	DrawItems(paintstruct, centerX, centerY);	
	RemoveFontResource("..\\GameData\\PiratesBay.ttf");
	DeleteObject(font);
}

void MenuView::DrawContent(PAINTSTRUCT* paintstruct, int centerX, int centerY)
{
	Models::Menu::const_content_iterator iterator = _menu->ContentBegin();
	Models::Menu::const_content_iterator end = _menu->ContentEnd();
	SetTextColor(paintstruct->hdc, RGB(40,30,20));
	while(iterator != end) 
	{
		TextOut(paintstruct->hdc, _textX, _currentY, (*iterator).c_str(), (*iterator).length());
		_currentY += _fontHeight;

		iterator++;
	}
	_currentY += _fontHeight / 2;
}

void MenuView::DrawBackground(PAINTSTRUCT* paintstruct, int centerX, int centerY)
{
	if(_menu->HasBackground())
	{
		HBRUSH brush = CreateSolidBrush(RGB(0, 0, 0));
		RECT rect = paintstruct->rcPaint;
		FillRect(paintstruct->hdc, &rect, brush);
		DeleteObject(brush);
	}

	_bitmapContext = CreateCompatibleDC(paintstruct->hdc);
	SelectObject(_bitmapContext, _menu->GetBackgroundImage());

	GdiTransparentBlt(paintstruct->hdc,
		centerX - 200,
		centerY - 160,
		400,
		320,
		_bitmapContext,
		paintstruct->rcPaint.top,
		paintstruct->rcPaint.left,
		400,
		320,
		RGB(255, 0, 255));

	DeleteDC(_bitmapContext);
}

void MenuView::DrawItems(PAINTSTRUCT* paintstruct, int centerX, int centerY)
{
	Models::Menu::const_menuitem_iterator iterator = _menu->MenuItemsBegin();
	Models::Menu::const_menuitem_iterator end = _menu->MenuItemsEnd();
	while(iterator != end) 
	{
		if(_menu->GetMenuItems()->at(_menu->GetSelectedIndex()) == *iterator)
		{
			SetTextColor(paintstruct->hdc, RGB(40,30,20));
			TextOut(paintstruct->hdc, _textX, _currentY, "x", 1);
			TextOut(paintstruct->hdc, _textX + _leftMargin, _currentY, (*iterator).c_str(), (*iterator).length());
		}
		else
		{
			SetTextColor(paintstruct->hdc, RGB(28,17,11));
			TextOut(paintstruct->hdc, _textX + _leftMargin, _currentY, (*iterator).c_str(), (*iterator).length());
		}

		iterator++;
		_currentY += _fontHeight;
	}
}