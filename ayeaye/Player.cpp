#include "stdafx.h"
#include "Player.h"
#include <ctime>

using namespace Models;


Player::Player(Point<float> position) 
{
	Point<float> v = { 200, 200 };
	_defaultVelocity = v;
	SetPosition(position);
	_score = 0;
	_spawnPoint = position;
	_hasWon = false;
	_lives = 3;
	_hasLost = false;
}

Keyboard* Player::GetKeyboard()	
{
	return &_keyboard;
}

Player::~Player()
{
}

Player* Player::MakeGameObject(const Loaders::GameObjectProperties& props)
{
	Point<float> playerPosition = { props.posx, props.posy };
	Models::Player * go = new Models::Player(playerPosition);
	Controllers::Animator * playerAnimator = new Controllers::PlayerAnimator(props.sprites, go);
	Controllers::PlayerController * ctrl = new Controllers::PlayerController();
	ctrl->SetPlayer(go);
	go->SetController(ctrl);
	go->SetSprite(playerAnimator);
	go->SetWidth(props.width);
	go->SetHeight(props.height);
	go->SetPosX(props.posx);
	go->SetPosY(props.posy);
	go->SetType(props.type);
	return go;
}
void Player::AddScore(int score)
{
	_score += score;
}

int Player::GetScore()
{
	return _score;
}

void Player::SetScore(int score)
{
	_score = score;
}
											   
void Player::Respawn()
{
	// ignore if function is called 
	// mutiple times in 0.5 secs
	static std::clock_t lastRespawnTime = 0;
	if (std::clock() - lastRespawnTime < 500)
		return;	   
	lastRespawnTime = std::clock();
	Die();
	if(!HasWon())
	{
		SetPosition(_spawnPoint);
		SetVelocity(_defaultVelocity);
		GetMovement()->Stop();
	}
}

void Player::Win()
{
	_hasWon = true;
}

bool Player::HasWon()
{
	return _hasWon;
}

Sound* Player::GetJumpSound()
{
	return &_jumpSound;
}

Sound* Player::GetDieSound()
{
	return &_dieSound;
}

void Player::Die()
{
	_lives--;
	if(_lives <= 0)
	{
		_hasLost = true;
	}
}

bool Player::HasLost()
{
	return _hasLost;
}

int Player::GetLives()
{
	return _lives;
}

void Player::SetLives(int lives)
{
	_lives = lives;
}