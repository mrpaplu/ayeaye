#include "stdafx.h"
#include "ScoreController.h"
#include <iostream>
#include <fstream>
#include <string>
#include <Windows.h>
#include <lmcons.h>

using namespace Controllers;

ScoreController::ScoreController(void)
{
	_highscores = new int [5];
	LoadHighscores();
	_scoreView = new Views::ScoreView();
}


ScoreController::~ScoreController(void)
{
}

void ScoreController::LoadHighscores()
{
	std::ifstream file;
	std::string line;

	char username[UNLEN+1];
	DWORD username_len = UNLEN+1;
	GetUserName(username, &username_len);

	std::string fileLocation = "..\\GameData\\";
	fileLocation += username;
	fileLocation += ".ayescore";
	file.open(fileLocation);
	if(file.fail())
	{
		std::cout << "No or corrupted highscores file" << std::endl;
		ResetHighscores();
		return;
	}

	int i = 0;
	while(std::getline(file, line, '\n'))
	{
		_highscores[i] = std::stoi(line);
		i++;
	}
	file.close();
}

void ScoreController::ResetHighscores()
{
	for(int i = 0; i < 5; i++)
	{
		_highscores[i] = 0;
	}
	SaveHighscores();
}

int* ScoreController::GetHighcores()
{
	return _highscores;
}

void ScoreController::SaveHighscores()
{
	std::ofstream file;

	char username[UNLEN+1];
	DWORD username_len = UNLEN+1;
	GetUserName(username, &username_len);

	std::string fileLocation = "..\\GameData\\";
	fileLocation += username;
	fileLocation += ".ayescore";
	file.open(fileLocation);

	if(file.fail())
	{
		std::cout << "Can't create new Highscores file!" << std::endl;
		return;
	}

	for(int i = 0; i < 5; i++)
	{
		file << std::to_string(_highscores[i]) << std::endl;
	}
	file.close();
}

void ScoreController::SetHighscore(int score)
{
	if(score > _highscores[4])
	{
		int temp = 0;
		_highscores[4] = score;
		for(int i = 3; i >= 0; i--)
		{
			if(_highscores[i] < _highscores[i+1])
			{
				temp = _highscores[i];
				_highscores[i] = _highscores[i+1];
				_highscores[i+1] = temp;
			}
			else 
			{
				break;
			}
		}
		SaveHighscores();
	}
}

Views::ScoreView* ScoreController::GetView()
{
	return _scoreView;
}