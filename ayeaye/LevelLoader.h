#pragma once

#include "Level.h"
#include "GameObjectLoader.h"
#include "BackgroundLoader.h"
#include "SoundLoader.h"

namespace Loaders
{
	class LevelLoader
	{
	public:
		LevelLoader();
		~LevelLoader();	 
		Models::Level Load(HWND, std::istream&, Models::Bitmap * spriteMapBmp);
	private:
		GameObjectLoader _gameObjectLoader;
	};	
}