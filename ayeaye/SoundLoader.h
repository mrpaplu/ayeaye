#pragma once
#include "SoundController.h"
#include <iostream>

namespace Loaders
{
	class SoundLoader
	{
		struct WaveHeaderType
		{
			char chunkId[4];
			unsigned long chunkSize;
			char format[4];
			char subChunkId[4];
			unsigned long subChunkSize;
			unsigned short audioFormat;
			unsigned short numChannels;
			unsigned long sampleRate;
			unsigned long bytesPerSecond;
			unsigned short blockAlign;
			unsigned short bitsPerSample;
			char dataChunkId[4];
			unsigned long dataSize;
		};
	public:
		SoundLoader(void);
		~SoundLoader(void);
		bool LoadWaveFile(std::string&, Models::Sound*, IDirectSound8*);
		bool LoadWaveFile(char*, Models::Sound*, IDirectSound8*);
	};
}