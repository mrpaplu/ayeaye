#pragma once
#include "animator.h"
namespace Controllers
{
	class RumAnimator :	public Animator
	{
	public:
		RumAnimator(std::vector<Models::Sprite*>, Models::GameObject*);
		~RumAnimator(void);
		void UpdateSprite(float delta);
	private:
		int _curOffset;
		float _timeout;
	};
}