#pragma once
#include "Sound.h"

namespace Views
{
	class SoundView
	{
	public:
		SoundView(void);
		~SoundView(void);
		bool Play(Models::Sound*);
		bool PlayLoop(Models::Sound*);
		void Stop(Models::Sound*);
	};
}
