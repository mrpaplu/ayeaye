#pragma once

#include "Menu.h"
#include "MainMenu.h"
#include "IngameMenu.h"

namespace Views 
{
	class MenuView
	{
	public:
		MenuView(void);
		MenuView(Models::Menu*);
		~MenuView(void);
		void Paint(HWND hWnd, PAINTSTRUCT*);
	private:
		HDC _bitmapContext;
		Models::Menu* _menu;
		void DrawBackground(PAINTSTRUCT*, int, int);
		void DrawItems(PAINTSTRUCT*, int, int);
		void DrawContent(PAINTSTRUCT*, int, int);
		int _fontHeight;
		int _currentY;
		int _textX;
		int _leftMargin;
	};
}