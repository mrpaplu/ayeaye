#include "stdafx.h"
#include "Utils.h"

using namespace Utils;

std::string Utils::Trim(std::string& s)
{
	s.erase(0, s.find_first_not_of(" \t"));
	s.erase(s.find_last_not_of(" \t") + 1);
	return s;
}

std::istream& Utils::getline(std::istream& i, std::string& line)
{
	std::getline(i, line);
	if (line.size() > 0 && line.back() == '\r')
		line.pop_back();
	return i;
}

bool Utils::CompareIgnoreCaseStrings(const std::string first, const std::string second) {
	return _stricmp(first.c_str(), second.c_str()) == 0;
}