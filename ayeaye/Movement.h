#pragma once

#include "Point.h"

namespace Models
{
	class Movement
	{
	public:
		void AlterMovement(Point<float>&);
		void Stop();
		void StopX();
		void StopY();
		Point<float> GetVelocity();
		void SetVelocity(Point<float>);
		Movement(void);
		~Movement(void);
	private:
		Point<float> _velocity;
	};
}