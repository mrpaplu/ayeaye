#pragma once

#include "SplashScreenView.h"
#include "SplashScreen.h"

namespace Controllers{
	class SplashScreenController
	{
	public:
		SplashScreenController();
		~SplashScreenController();
		void Paint(HWND window, PAINTSTRUCT* ps);
		Models::SplashScreen* SplashScreenController::GetSplashScreen();
		void TurnSplashScreenOn();
		void TurnSplashScreenOff();
	private:
		HBITMAP _image;
		Views::SplashScreenView* _splashScreenView;
		Models::SplashScreen* _splashScreen;
	};
}