#include "stdafx.h"
#include "Viewport.h"

using namespace Models;

Viewport::Viewport(void)
{
	Point<int> p = {0, 0};
	SetPosition(p);
	SetSize(p);
}

Viewport::Viewport(Point<int> size)
{
	Point<int> p = {0, 0};
	SetPosition(p);
	SetSize(size);
}

Viewport::Viewport(Point<int> pos, Point<int> size)
{
	SetPosition(pos);
	SetSize(size);
}

void Viewport::SetPosition(Point<int> pos)
{
	_position = pos;
}
		
void Viewport::SetSize(Point<int> s)
{
	_size = s;
}
		
Point<int> Viewport::GetPosition()
{
	return _position;
}
	
Point<int> Viewport::GetSize()
{
	return _size;
}

Viewport::~Viewport(void)
{

}

