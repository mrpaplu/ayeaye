#include "stdafx.h"
#include "Menu.h"

using namespace Models;

Menu::Menu(void)
{
	_isActive = false;
	_selectedItem = 0;
	_isToggleable = false;
	_width = 200;
	_height = 100;
	_backgroundImage = (HBITMAP) LoadImage(NULL, 
		"..\\GameData\\menu_bg.bmp", 
		IMAGE_BITMAP, 
		0, 0, 
		LR_LOADFROMFILE);
}


Menu::~Menu(void)
{
}

std::vector<std::string>* Menu::GetContent()
{
	return &_content;
}

void Menu::SetContent(bool hasContent)
{
	_hasContent = hasContent;
}

bool Menu::HasContent()
{
	return _hasContent;
}

bool Menu::IsActive()
{
	return _isActive;
}

void Menu::SetActive(bool active)
{
	_isActive = active;
	if(!_isActive)
	{
		_selectedItem = 0;
	}
}

std::vector<std::string>* Menu::GetMenuItems()
{
	return &_items;
}

Menu::const_menuitem_iterator Menu::MenuItemsBegin() const
{
	return _items.begin();
}

Menu::const_menuitem_iterator Menu::MenuItemsEnd() const
{
	return _items.end();
}

Menu::const_content_iterator Menu::ContentBegin() const
{
	return _content.begin();
}

Menu::const_content_iterator Menu::ContentEnd() const
{
	return _content.end();
}

int Menu::GetSelectedIndex()
{
	return _selectedItem;
}

void Menu::SelectPrevious()
{
	_selectedItem--;
	if(_selectedItem < 0)
	{
		_selectedItem = _items.size() - 1;
	}
}
		
void Menu::SelectNext()
{
	_selectedItem++;
	if(_selectedItem >= _items.size())
	{
		_selectedItem = 0;
	}
}

bool Menu::HasBackground()
{
	return _hasBackground;
}

int Menu::GetHeight()
{
	return _height;
}

int Menu::GetWidth()
{
	return _width;
}

HBITMAP Menu::GetBackgroundImage()
{
	return _backgroundImage;
}

bool Menu::IsToggleable()
{
	return _isToggleable;
}
