#include "stdafx.h"
#include "BackgroundView.h"

using namespace Views;

BackgroundView::BackgroundView(void)
{
}


BackgroundView::~BackgroundView(void)
{
}

void BackgroundView::Paint(PAINTSTRUCT* ps, Controllers::ViewportController& viewportController, Models::Background* background)
{	
	if(background == NULL)
		return;
		Models::Sprite* sprite;
	Point<int> position = {0, 0};
	int width;
	int height;
	int num_paints;
	int* translatedXPositions;	

	for (int bgNumber=0; bgNumber<2; bgNumber++)
	{
		sprite = background->GetSprite(bgNumber);
		
		if (sprite == NULL)
			return;
		
		position.x = 0;
		if(bgNumber==1)
			position.y = background->GetMaxY() - sprite->GetHeight();
		else 
			position.y = -25;

		width = sprite->GetWidth();
		height = sprite->GetHeight();
		translatedXPositions = viewportController.TranslateBackground(bgNumber, position, num_paints, width, height);

		for(int i=0; i<num_paints; i++)
		{
			sprite->Render(ps->hdc, translatedXPositions[i], position.y);
		}
		delete [] translatedXPositions;
	}
}