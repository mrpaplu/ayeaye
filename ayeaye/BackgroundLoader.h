#pragma once

#include <string>
#include <fstream>
#include "LevelLoaderError.h"  
#include "Utils.h"
#include "ZipArchive.h"
#include "Background.h"

namespace Loaders
{
	class BackgroundLoader
	{
	public:
		BackgroundLoader(void);
		~BackgroundLoader(void);
		static void SetFromType(HWND, Models::Sprite** , const std::string&);
		static Models::Background* LoadBackground(HWND window, std::istream& input);
	};
}