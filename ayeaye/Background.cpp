#include "stdafx.h"
#include "Background.h"

using namespace Models;

Background::Background(void)
{
}

Background::~Background(void)
{
	delete [] _sprite;
}

void Background::SetSprite(int i, Sprite* sprite)
{
	_sprite[i] = sprite;
}

Sprite* Background::GetSprite(int i)
{
	return _sprite[i];
}

void Background::SetMaxY(int i)
{
	_maxY = i;
}

int Background::GetMaxY()
{
	return _maxY;
}

