#include "stdafx.h"
#include "ZipArchive.h"

#include <istream>
#include <string>

using namespace Loaders;

ZipArchive::ZipArchive(std::string path)
{
	int error;
	_archive = zip_open(path.c_str(), NULL, &error);
	if (_archive == NULL)
	{
		throw ZipException(error);
	}
	int numFiles = zip_get_num_entries(_archive, NULL);
	for (int i = 0; i < numFiles; i++)
	{
		struct zip_stat* fileInfo = new struct zip_stat;
		zip_stat_index(_archive, i, NULL, fileInfo);
		_files.push_back(new ZipFile(_archive, fileInfo));
	}
}

ZipArchive::~ZipArchive()
{
	std::vector<ZipFile*>::iterator it;
	for (it = _files.begin(); it != _files.end(); it++)
	{
		delete *it;
	}
	if (_archive)
		zip_close(_archive);
}
Loaders::ZipArchive::const_iterator ZipArchive::begin() const
{
	return _files.begin();
}
Loaders::ZipArchive::const_iterator ZipArchive::end() const
{
	return _files.end();
}

std::streambuf* ZipArchive::Open(const std::string& fname)
{
	std::vector<ZipFile*>::iterator it;
	for (it = _files.begin(); it != _files.end(); it++)
	{
		if (_stricmp((*it)->Name().c_str(), fname.c_str()) == 0)
			return (*it)->CreateInputStream();
	}
	throw ZipException("File not found int the archive");
}

bool ZipArchive::FileExists(const std::string& fname)
{
	std::vector<ZipFile*>::iterator it;
	for (it = _files.begin(); it != _files.end(); it++)
	{
		if (_stricmp((*it)->Name().c_str(), fname.c_str()) == 0)
			return true;
	}
	return false;
}


ZipFile::ZipFile(struct zip* archive, struct zip_stat* info)
{
	_info = info;
	_myArchive = archive;
}

ZipFile::~ZipFile()
{
	std::vector<std::streambuf*>::iterator it;
	for (it = _streambufs.begin(); it != _streambufs.end(); it++)
	{
		delete *it;
	}
	if (_info)
		delete _info;
}

std::string ZipFile::Name()
{
	return std::string(_info->name);
}

std::streambuf* ZipFile::CreateInputStream()
{
	ZipFileBuffer *buffer = new ZipFileBuffer(_myArchive, _info);
	_streambufs.push_back(buffer);
	return buffer;
}					

int Loaders::ZipFile::Index()
{
	return _info->index;
}

ZipFileBuffer::ZipFileBuffer(struct zip* archive, struct zip_stat* info)
: _buffer(_maxBufferSize)
{								
	_file = zip_fopen_index(archive, info->index, NULL);
	char* end = &_buffer.front() + _buffer.size();
	setg(end, end, end);  	
}

ZipFileBuffer::~ZipFileBuffer()
{
	if (_file)
		zip_fclose(_file);
}

ZipFileBuffer::int_type ZipFileBuffer::underflow()
{
	// return current char if there 
	// is atleast one in the buffer
	if (gptr() < egptr())
	{
		return traits_type::to_int_type(*gptr());
	}

	// Else read the next bytes from the file
	std::streamsize n = zip_fread(_file, &_buffer.front(), _buffer.size());
	
	// Return EOF if nothing was read
	if (n == 0)
	{
		return traits_type::eof();
	}

	// update pointers
	setg(&_buffer.front(), &_buffer.front(), &_buffer.front() + n);
	
	// return current
	return traits_type::to_int_type(*gptr());
}
extern int errno;

ZipException::ZipException(std::string what)
{
	_message = what;
}

ZipException::ZipException(int ze)
{
	char buff[1024];
	int se = ze;
	if (!errno)
		se = errno;
	int len = zip_error_to_str(buff, 1023, ze, se);
	_message = std::string(buff);
}

const char* ZipException::what() const
{
	return _message.c_str();
}
