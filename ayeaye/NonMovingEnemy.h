#pragma once
#include "Enemy.h"

namespace Models
{
	class NonMovingEnemy : public Enemy
	{
	public:
		NonMovingEnemy(void);
		~NonMovingEnemy(void);
	};
}
