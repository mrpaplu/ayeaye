#include "stdafx.h"
#include "MainMenu.h"

using namespace Models;

MainMenu::MainMenu(void)
{
	SetActive(true);
	_hasBackground = true;
	_hasContent = true;
}


MainMenu::~MainMenu(void)
{
}

void MainMenu::SetMenuItems()
{
	_items.push_back("New game");
	_items.push_back("Highscores");
	_items.push_back("Load game");
	_items.push_back("Quit");
}