#pragma once
#include "Background.h"
#include "ViewportController.h"

namespace Views
{
	class BackgroundView
	{
	public:
		BackgroundView(void);
		~BackgroundView(void);
		void Paint(PAINTSTRUCT*, Controllers::ViewportController&, Models::Background*);
	private:
		HDC _bitmapContext;
	};
}
