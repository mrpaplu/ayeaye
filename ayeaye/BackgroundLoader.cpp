#include "stdafx.h"
#include "BackgroundLoader.h"
#include "Bitmap.h"

using namespace Loaders;

BackgroundLoader::BackgroundLoader(void)
{
}


BackgroundLoader::~BackgroundLoader(void)
{
}

void BackgroundLoader::SetFromType(HWND window, Models::Sprite** sprites, const std::string& typeName)
{
	const std::string goBasePath = "..\\GameData\\GameObjects";
	std::string filePath = goBasePath + "\\" + typeName + ".arg";
	std::string goFile = typeName + ".txt";
	Loaders::ZipArchive zip(filePath);
	std::istream file(zip.Open(goFile));
	std::string line;
	Utils::getline(file, line);
	if (Utils::Trim(line) != "BEGIN:BACKGROUND")
		throw LevelLoad_error();
	while (Utils::getline(file, line))
	{
		std::string key = Utils::Trim(line.substr(0, line.find(':')));
		std::string value = Utils::Trim(line.substr(line.find(':') + 1));
		if (key == "END" && value == "BACKGROUND")	   
		{
			std::istream sprite1IStream(zip.Open("sprite1.bmp"));
			std::istream sprite2IStream(zip.Open("sprite2.bmp"));
			Models::Bitmap* bmp1 = new Models::Bitmap(window, sprite1IStream);
			Models::Bitmap* bmp2 = new Models::Bitmap(window, sprite2IStream);
			RECT rect1;
			RECT rect2;
			rect1.top = rect1.left = rect2.top = rect2.left = 0;
			rect1.bottom = bmp1->GetHeight();
			rect1.right = bmp1->GetWidth();
			rect2.bottom = bmp2->GetHeight();
			rect2.right = bmp2->GetWidth();
			sprites[0] = new Models::Sprite(rect1, bmp1);
			sprites[1] = new Models::Sprite(rect2, bmp2);
			return;
		}
		else if (key == "TYPE"){} //ignore
		else
			throw LevelLoad_error();
	}
	throw LevelLoad_error();
}

Models::Background* BackgroundLoader::LoadBackground(HWND window, std::istream& input)
{
	std::string line;
	Models::Sprite* sprites[2];
	while (Utils::getline(input, line))
	{
		std::string key = Utils::Trim(line.substr(0, line.find(':')));
		std::string value = Utils::Trim(line.substr(line.find(':') + 1));
		if (key == "END" && value == "BACKGROUND")
		{
			Models::Background* bg = new Models::Background();
			bg->SetSprite(0, sprites[0]);
			bg->SetSprite(1, sprites[1]);
			return bg;
		}	
		else if (key == "TYPE")
			SetFromType(window, sprites, value);
		else
			throw LevelLoad_error();
	}
	throw LevelLoad_error();
}