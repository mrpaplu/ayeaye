#pragma once
#include "Animator.h"

namespace Controllers
{
	class BlackbeardAnimator : public Animator
	{
	public:
		BlackbeardAnimator(std::vector<Models::Sprite*>, Models::GameObject*);
		~BlackbeardAnimator(void);
		void UpdateSprite(float delta);
	private:
		int _curOffset;
		float _timeout;
		bool _isLeft;
	};
}