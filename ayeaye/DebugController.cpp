#include "stdafx.h"
#include "DebugController.h"

using namespace Controllers;

DebugController::DebugController()
{
}

DebugController::DebugController(Models::DebugInfo* debugInfo)
{
	_debugInfo = debugInfo;
}

DebugController::~DebugController()
{
}

void DebugController::ToggleDebugMode()
{
	_debugInfo->DebugOn = !_debugInfo->DebugOn;
}