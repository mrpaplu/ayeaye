#pragma once

#include <Windows.h>
#include <string>
#include <istream>
#include <map>

namespace Models
{
	class Bitmap;
	class BitmapStore
	{
	public:
		static BitmapStore* GetInstance();
		Bitmap* LoadBitmapFromInput(std::string name, HWND hWnd, std::istream& is);
		Bitmap* GetBitmap(std::string name);
		bool IsLoaded(std::string name);
		void EmptyStore();
		~BitmapStore();
	private:
		BitmapStore();
		std::map<std::string,Bitmap*> _loadedBitmaps;
	};

	class Bitmap
	{			  
	public:
		Bitmap(HWND hwnd, std::istream& is);
		~Bitmap();
		int GetWidth() const;
		int GetHeight() const;
		void Render(HDC hdc, int srcX, int srcY, int dstX, int dstY, int srcW, int srcH) const;
	private:
		int _width;
		int _height;
		HBITMAP _bitmap;
	};
}