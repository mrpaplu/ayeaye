#include "stdafx.h"
#include "GameObjectFactory.h"
#include "Player.h"
#include "PlayerAnimator.h"
#include "PlayerController.h"
#include "RumAnimator.h"
#include "BlackbeardAnimator.h"
#include "Blackbeard.h"
#include "Rum.h"
#include "BlackbeardController.h"
#include "Portal.h"
#include "PortalAnimator.h"
#include "Vodka.h"

Models::GameObject* Loaders::GameObjectFactory::Create(const GameObjectProperties& props)
{
	if (Utils::CompareIgnoreCaseStrings(props.type, "PLAYER"))
	{
		return Models::Player::MakeGameObject(props);
	}
	else if(Utils::CompareIgnoreCaseStrings(props.type, "RUM"))
	{
		return Models::Rum::MakeGameObject(props);
	}	
	else if (Utils::CompareIgnoreCaseStrings(props.type, "VODKA"))
	{
		return Models::Vodka::MakeGameObject(props);
	}
	else if(Utils::CompareIgnoreCaseStrings(props.type, "Blackbeard"))
	{
		return Models::Blackbeard::MakeGameObject(props);
	}
	else if(Utils::CompareIgnoreCaseStrings(props.type, "Portal"))
	{
		return Models::Portal::MakeGameObject(props);
	}
	else
	{
		return Models::GameObject::MakeGameObject(props);
	}
	return NULL;
}