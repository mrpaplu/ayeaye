#pragma once

#include "DebugInfoView.h"
#include "DebugInfo.h"

namespace Controllers{
	class DebugController
	{
	public:
		DebugController();
		DebugController(Models::DebugInfo*);
		~DebugController();
		void ToggleDebugMode();
	private:
		Models::DebugInfo* _debugInfo;
	};
}