#include "stdafx.h"
#include "PlayerAnimator.h"

using namespace Controllers;
												
PlayerAnimator::PlayerAnimator(std::vector<Models::Sprite*> sprites, Models::MovingGameObject* player)
: Animator(sprites, player), _curOffset(0), _timeout(0)
{
	if (sprites.size() != 6)
	{
		throw new std::exception("Sprites should contain 6 sprites for PlayerAnimator");
	}
}

PlayerAnimator::~PlayerAnimator()
{
}

void PlayerAnimator::UpdateSprite(float delta)
{
	if (!_ourObject){
		return;
	}

	_timeout += delta;
	Point<float> velocity = _ourObject->GetVelocity();

	if (velocity.x != 0 && _timeout >= 0.2)
	{
		_timeout = 0;
		_curOffset++;
		if (_curOffset >= 3)
			_curOffset = 0;
	}
	else if (velocity.x == 0)
	{				 
		_curOffset = 1;
	}

	if (velocity.x < 0)
	{
		_isLeft = true;
	}
	else if (velocity.x > 0)
	{
		_isLeft = false;
	}
	
	if (_isLeft)
	{
		_currentSprite = _sprites[_curOffset];
	}
	else
	{
		_currentSprite = _sprites[3 + _curOffset];
	}
}