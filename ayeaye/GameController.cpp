#include "stdafx.h"
#include <ctime>
#include <iostream>
#include <lmcons.h>
#include "GameController.h"
#include "Player.h"
#include "LevelLoader.h"
#include "ZipArchive.h"
#include "Bitmap.h"
#include "SoundLoader.h"

using namespace Controllers;

GameController::GameController(void)
{
}

GameController::GameController(HWND window) : _soundController(window)
{
	_currentLevel = 0;
	_levels.push_back("beach");
	_levels.push_back("jungle");
	_levels.push_back("ocean");
	_levels.push_back("town");
	_levels.push_back("space");


	_game = new Models::Game();

	_debugInfoModel = new Models::DebugInfo();

	_gameObjectView = Views::GameObjectView();
	_debugInfoView = Views::DebugInfoView(_debugInfoModel);
	_debugController = DebugController(_debugInfoModel);
	_splashScreenController = SplashScreenController();
	_scoreController = ScoreController();
		
	_menuController.SetActive(false);
	Loaders::SoundLoader soundLoader;
	_menuController.SetSoundController(&_soundController);
	soundLoader.LoadWaveFile("..\\GameData\\Sounds\\menumusic.wav", _menuController.GetMusic(), _soundController.GetDirectSound());

	_window = window;
	_framePerSecond = 60;
	_timePassedAnimation = 0;
	_timePassedMoving = 0;
	_maxSleep = 1000 / _framePerSecond;
}

GameController::~GameController()
{
}

GameController::GameController(Models::Level level, HWND window, Point<int> windowSize) : _soundController(window)
{
	_game = new Models::Game();

	_debugInfoModel = new Models::DebugInfo();

	_level = level;
	_gameObjectView = Views::GameObjectView();
	_debugInfoView = Views::DebugInfoView(_debugInfoModel);
	_debugController = DebugController(_debugInfoModel);
	_collisionController = CollisionController(&_level);
	_splashScreenController = SplashScreenController();

	_menuController.SetActive(false);
	Loaders::SoundLoader soundLoader;
	_menuController.SetActive(false);
	_menuController.SetSoundController(&_soundController);
	Models::Sound* music = _menuController.GetMusic(); 
	soundLoader.LoadWaveFile("..\\GameData\\Sounds\\menumusic.wav", music, _soundController.GetDirectSound());


	_window = window;
	_framePerSecond = 60;
	_timePassedAnimation = 0;
	_timePassedMoving = 0;
	_maxSleep = 1000 / _framePerSecond;
	InitializeCollisionBoxes();

	_viewportController = ViewportController(windowSize);
}

void GameController::PlayLevelMusic()
{
	_soundController.GetView()->PlayLoop(_level.GetMusic());
}

void GameController::StopLevelMusic()
{
	_soundController.GetView()->Stop(_level.GetMusic());
}

void GameController::Paint(PAINTSTRUCT* ps)
{
	Models::Level::const_gameobjects_iterator iterator = _level.GameObjectsBegin();
	Models::Level::const_gameobjects_iterator end = _level.GameObjectsEnd();
	
	if (!_menuController.GetMenu()->HasBackground())
	{
		_backgroundView.Paint(ps, _viewportController, _level.GetBackground());
	}
	while(iterator != end) 
	{
		_gameObjectView.Paint(ps, _viewportController, *iterator);
		_debugInfoView.PaintObjectInfo(ps, _viewportController, *iterator);
		iterator++;
	}
	_debugInfoView.PaintGeneralInfo(ps, _viewportController);
	if (_game->GetGameState() == Models::GameState::Paused){
		_menuController.Paint(_window, ps);
	}
	else if (_game->GetGameState() == Models::GameState::Ended ||
		_game->GetGameState() == Models::GameState::Started ||
		_game->GetGameState() == Models::GameState::Lost ||
		_game->GetGameState() == Models::GameState::Loading ||
		_game->GetGameState() == Models::GameState::LoadingFirstLevel ||
		_game->GetGameState() == Models::GameState::Credits)

	_splashScreenController.Paint(_window, ps);
	{
		_splashScreenController.Paint(_window, ps);
	}
	if(_game->GetGameState() == Models::GameState::Playing && GetPlayerController() != NULL)
	{
		_scoreController.GetView()->Paint(ps, _viewportController, GetPlayerController()->GetPlayer()->GetScore(), GetPlayerController()->GetPlayer()->GetLives(), std::clock() - _lastFrameTime);
	}
}

void GameController::InitializeCollisionBoxes()
{
	Models::Level::gameobjects_iterator iterator = _level.GameObjectsBegin();
	Models::Level::gameobjects_iterator end = _level.GameObjectsEnd();
	while(iterator != end) 
	{
		(*iterator)->SetCollisionBox();
		iterator++;
	}
}

void GameController::UpdateCollisionBox(Models::GameObject* gameObject)
{
	gameObject->SetCollisionBox();
}

void GameController::SetLastFrameTimeToCurrentTime()
{
	_lastFrameTime = std::clock();
}

void GameController::CheckGameStates()
{
	if (_splashScreenController.GetSplashScreen()->IsActive() 
		&& _game->GetGameState() != Models::GameState::Ended
		&& _game->GetGameState() != Models::GameState::Lost
		&& _game->GetGameState() != Models::GameState::Loading
		&& _game->GetGameState() != Models::GameState::LoadingFirstLevel
		&& _game->GetGameState() != Models::GameState::Credits)
	{
		_game->SetGameState(Models::GameState::Started);
		_splashScreenController.GetSplashScreen()->SplashScreenTextStarted();
		_menuController.PlayMusic();
	}
	else if (_game->GetGameState() == Models::GameState::Started)
	{
		_menuController.SetActive(true);
		_game->SetGameState(Models::GameState::Paused);
	}
	else if (_menuController.GetMenu()->IsActive() && _game->GetGameState() != Models::GameState::Ended)
	{
		_game->SetGameState(Models::GameState::Paused);
	}
	else if (_game->GetGameState() == Models::GameState::Ended)
	{
		_splashScreenController.GetSplashScreen()->SplashScreenTextEnded();
		_splashScreenController.TurnSplashScreenOn();
		_menuController.SetActive(false);
	}
	else if (_game->GetGameState() == Models::GameState::Lost)
	{
		_splashScreenController.GetSplashScreen()->Lose();
		_splashScreenController.TurnSplashScreenOn();
		StopLevelMusic();
		_menuController.PlayMusic();
		_menuController.SetActive(false);
	}
	else if (_game->GetGameState() == Models::GameState::Credits)
	{
		_menuController.PlayMusic();
		_splashScreenController.GetSplashScreen()->Credits();
		_splashScreenController.TurnSplashScreenOn();
		_menuController.SetActive(false);
	}
	else if (_game->GetGameState() == Models::GameState::Exited 
		&& !_splashScreenController.GetSplashScreen()->IsActive())
	{
		std::exit(0);
	}
	else if(_game->GetGameState() == Models::GameState::Playing && GetPlayerController() != NULL && GetPlayerController()->GetPlayer()->HasWon())
	{
		_game->SetGameState(Models::GameState::Won);
	}
	else if (_game->GetGameState() == Models::GameState::Playing && GetPlayerController() != NULL && GetPlayerController()->GetPlayer()->HasLost())
	{
		_game->SetGameState(Models::GameState::Lost);
	}
	else if(_game->GetGameState() == Models::GameState::Won)
	{
		StopLevelMusic();
		if(HasNextLevel())
		{
			_currentLevel += 1;
			_splashScreenController.GetSplashScreen()->GetLevelSplash(_levels.at(_currentLevel));
		    _splashScreenController.TurnSplashScreenOn();
			_game->SetGameState(Models::GameState::Loading);
			_menuController.SetActive(false);
		}
		else 
		{
			// Credits
			_scoreController.SetHighscore(GetPlayerController()->GetPlayer()->GetScore());
			_game->SetGameState(Models::GameState::Credits);
			
		}
	}
	else if(_game->GetGameState() == Models::GameState::NewGame)
	{
		SetCurrentLevel(0);
		std::string levelName = _levels.at(0);
		_splashScreenController.GetSplashScreen()->GetLevelSplash(levelName);
		_splashScreenController.TurnSplashScreenOn();
		_game->SetGameState(Models::GameState::LoadingFirstLevel);
	}
	else if(_game->GetGameState() == Models::GameState::Loading)
	{
		int score = GetPlayerController()->GetPlayer()->GetScore();
		QuitLevel();
		LoadLevel(_levels.at(_currentLevel), score);
		_splashScreenController.TurnSplashScreenOff();
		_game->SetGameState(Models::GameState::Playing);
	}
	else if(_game->GetGameState() == Models::GameState::LoadingFirstLevel)
	{
		_levels.at(_currentLevel);

		LoadLevel(_levels.at(_currentLevel), 0);
		_splashScreenController.TurnSplashScreenOff();
		_game->SetGameState(Models::GameState::Playing);
	}
	else 
	{
		_game->SetGameState(Models::GameState::Playing);
	}
}

void GameController::GameLoop()
{
	CheckGameStates();

	int sleeptime = 0;
	long startTime = std::clock();
	float delta = (float)((startTime - _lastFrameTime)) / CLOCKS_PER_SEC;
	_lastFrameTime = startTime;
	
	if (_game->GetGameState() == Models::GameState::Playing)
	{
		Models::Level::const_gameobjects_iterator it;
		// Update logic
		_level.DeleteDeletedObjects();
		for (it = _level.GameObjectsBegin(); it != _level.GameObjectsEnd(); it++)
		{
			Controllers::GameObjectController *ctrl = (*it)->GetController();
			if (ctrl != NULL)
			{
				ctrl->Update(delta);
			}
		}

		// Update Animators
		for (it = _level.GameObjectsBegin(); it != _level.GameObjectsEnd(); it++)
		{
			(*it)->GetSprite()->UpdateSprite(delta);
		}		

		// Check collision
		_collisionController.CheckCollisions(delta);
	}

	if (_game->GetGameState() == Models::GameState::Playing)
	{
		Point<float> p = _level.GetPlayer()->GetPosition();
		_viewportController.Update(p, _level.GetHeight(), _level.GetWidth());
	}
	RedrawWindow(_window, NULL, NULL, RDW_ERASE | RDW_UPDATENOW | RDW_INVALIDATE);

	long frameTime = std::clock() - startTime;

	sleeptime = _maxSleep - frameTime;

	if (sleeptime > 0) 
	{
		Sleep(sleeptime);
	}

	frameTime = std::clock();
	frameTime -= startTime;
	_timePassedAnimation += frameTime;
	_timePassedMoving += frameTime;
	_debugInfoModel->SetFPS(1000 / frameTime);
	_debugInfoModel->SetViewportPosition(_viewportController.GetViewport()->GetPosition());
}

PlayerController* GameController::GetPlayerController()
{
	if (_level.GetPlayer() == NULL)
		return NULL;
	return (PlayerController*)(_level.GetPlayer()->GetController());
}

DebugController* GameController::GetDebugController()
{
	return &_debugController;
}

ViewportController* GameController::GetViewportController()
{
	return &_viewportController;
}

MenuController* GameController::GetMenuController()
{
	return &_menuController;
}

SplashScreenController* GameController::GetSplashScreenController()
{
	return &_splashScreenController;
}

Models::Game* GameController::GetGame()
{
	return _game;
}

SoundController* GameController::GetSoundController()
{
	return &_soundController;
}

void GameController::LoadLevel(std::string levelName, int score)
{
	Loaders::ZipArchive zip("..\\GameData\\Levels\\" + levelName + ".aye");
	std::istream file(zip.Open(levelName + ".txt")); 
	std::istream spritemapFile (zip.Open("spritemap.bmp"));
	Models::Bitmap *spriteMap = new Models::Bitmap(_window, spritemapFile);
    Loaders::LevelLoader ll;
    Models::Level loadedLevel = ll.Load(_window, file, spriteMap);
	Loaders::SoundLoader sl;
	_level = loadedLevel;
	_level.SetLevelName(levelName);
	_collisionController = CollisionController(&_level);
	GetPlayerController()->GetPlayer()->SetScore(score);
	SetLastFrameTimeToCurrentTime();
	Models::Sound* playerJumpSound = loadedLevel.GetPlayer()->GetJumpSound();
	Models::Sound* playerDieSound = loadedLevel.GetPlayer()->GetDieSound();
	sl.LoadWaveFile("..\\GameData\\Sounds\\die.wav", playerDieSound, _soundController.GetDirectSound()); 
	sl.LoadWaveFile("..\\GameData\\Sounds\\jump.wav", playerJumpSound, _soundController.GetDirectSound()); 
	sl.LoadWaveFile("..\\GameData\\Sounds\\" + levelName + ".wav", _level.GetMusic(), _soundController.GetDirectSound());
	sl.LoadWaveFile("..\\GameData\\Sounds\\rum.wav", GetPlayerController()->GetRumSound(), _soundController.GetDirectSound());
	sl.LoadWaveFile("..\\GameData\\Sounds\\heartbeat.wav", GetPlayerController()->GetHeartSound(), _soundController.GetDirectSound());
	_soundController.GetView()->PlayLoop(_level.GetMusic());
	GetPlayerController()->SetSoundController(&_soundController);
}

void GameController::QuitLevel()
{
	//delete _level.GetBackground();
	_level.DestroyLevel();
}

void GameController::SaveGame()
{
	std::ofstream file;

	char username[UNLEN+1];
	DWORD username_len = UNLEN+1;
	GetUserName(username, &username_len);

	std::string fileLocation = "..\\GameData\\";
	fileLocation += username;
	fileLocation += ".ayesave";
	file.open(fileLocation);

	if(file.fail())
	{
		std::cout << "Can't save level!" << std::endl;
		return;
	}
	file << _level.GetLevelName() << std::endl;
	file << GetPlayerController()->GetPlayer()->GetScore() << std::endl;
	file.close();
}

std::string* GameController::GetSavedGame()
{
	std::ifstream file;
	std::string line;
	std::string* params = new std::string[2];

	char username[UNLEN+1];
	DWORD username_len = UNLEN+1;
	GetUserName(username, &username_len);

	std::string fileLocation = "..\\GameData\\";
	fileLocation += username;
	fileLocation += ".ayesave";
	file.open(fileLocation);
	if(file.fail())
	{
		std::cout << "No or corrupted save file" << std::endl;
		params[0] = _levels.at(0);
		params[1] = "0";
		return params;
	}

	
	int i = 0;
	while(std::getline(file, line, '\n') && i < 2)
	{
		params[i] = line;
		i++;
	}
	return params;
}

std::vector<std::string> GameController::GetLevelNames()
{
	return _levels;
}

bool GameController::HasNextLevel()
{
	return _currentLevel < _levels.size() - 1;
}

void GameController::SetCurrentLevel(int index)
{
	_currentLevel = index;
}
