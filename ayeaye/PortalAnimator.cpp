#include "stdafx.h"
#include "PortalAnimator.h"
#include "Animator.h"

using namespace Controllers;

PortalAnimator::PortalAnimator(std::vector<Models::Sprite*> sprites, Models::GameObject* ourObject)
	: Animator(sprites, ourObject), _curOffset(0), _timeout(0)
{
}


PortalAnimator::~PortalAnimator(void)
{
}

void PortalAnimator::UpdateSprite(float delta)
{
	if (!_ourObject)
	{
		return;
	}

	_timeout += delta;

	if (_timeout >= 0.15)
	{
		_timeout = 0;
		_curOffset++;
		if (_curOffset >= 2)
		{
			_curOffset = 0;
		}
	}
	_currentSprite = _sprites[_curOffset];
}