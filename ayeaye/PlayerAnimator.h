#pragma once

#include "Sprite.h"
#include "Player.h"

namespace Controllers
{
	class PlayerAnimator : public Animator
	{
	public:
		PlayerAnimator(std::vector<Models::Sprite*> sprites, Models::MovingGameObject* player);
		~PlayerAnimator();
		void UpdateSprite(float delta);
	private:
		int _curOffset;
		bool _isLeft;
		float _timeout;
	};
}