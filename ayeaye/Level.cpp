#include "stdafx.h"
#include "Level.h"

#include <algorithm>

using namespace Models;
Level::Level()
{
}

void Level::DestroyLevel()
{
	Models::Level::gameobjects_iterator iterator;

	for (iterator = GameObjectsBegin(); iterator != GameObjectsEnd(); iterator++)
	{
		delete *iterator;
		(*iterator) = NULL;
	}
	SetPlayer(NULL);
	_gameObjects.clear();
}

Level::~Level()
{
}

int Level::GetWidth() const
{
	return _width;
}

int Level::GetHeight() const
{
	return _height;
}

void Level::SetWidth(int w)
{
	_width = w;
}

void Level::SetHeight(int h)
{
	_height = h;
}

Level::const_gameobjects_iterator Level::GameObjectsBegin()	const
{
	return _gameObjects.begin();
}

Level::const_gameobjects_iterator Level::GameObjectsEnd() const
{
	return _gameObjects.end();
}

Level::gameobjects_iterator Level::GameObjectsBegin()
{
	return _gameObjects.begin();
}

Level::gameobjects_iterator Level::GameObjectsEnd() 
{
	return _gameObjects.end();
}

void Level::AddGameObject(Models::GameObject* go)
{
	_gameObjects.push_back(go);
}

void Level::SetPlayer(Player* p)
{
	_player = p;
}

void Level::SetBackground(Background* bg)
{
	_background = bg;
	_background->SetMaxY(GetHeight());
}

Player* Level::GetPlayer()
{
	return _player;
}

Background* Level::GetBackground()
{
	return _background;
}

bool IsMarkedForDeletion(GameObject* g)
{
	return g->IsMarkedForDeletion();
}

void Level::DeleteDeletedObjects()
{
	_gameObjects.erase(std::remove_if(_gameObjects.begin(), _gameObjects.end(), IsMarkedForDeletion), _gameObjects.end());
}

void Level::SetLevelName(std::string s)
{
	_levelName = s;
}

std::string Level::GetLevelName()
{
	return _levelName;
}

Sound* Level::GetMusic()
{
	return &_bgMusic;
}