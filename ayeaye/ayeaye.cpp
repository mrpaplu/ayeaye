#include "stdafx.h"
#include "ayeaye.h"
#include "GameController.h"
#include "GameEventHandler.h"

#include <fstream>
#include <iostream>	  
#include <iterator>
#include <algorithm>
#include <string>


#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE InstanceHandler;				// current instance
TCHAR WindowTitle[MAX_LOADSTRING];			// The title bar text
TCHAR WindowClassName[MAX_LOADSTRING];			// the main window class name
Controllers::GameController* GameController;
Controllers::GameEventHandler* GameEventHandler;
HDC backBuffer;
HBITMAP memoryBitmap, memoryOldBitmap;
Point<int> windowSize;
RECT levelRect;
RECT Client_Rect;

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE);
BOOL				InitializeInstance(HINSTANCE, int);
LRESULT CALLBACK	WindowProcedure(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE instance,
                     _In_opt_ HINSTANCE previousInstance,
                     _In_ LPTSTR    commandLine,
                     _In_ int       commandShow)
{
	UNREFERENCED_PARAMETER(previousInstance);
	UNREFERENCED_PARAMETER(commandLine);

#ifdef _DEBUG
	AllocConsole();
	//Re-assign stdout to the new allocated console.
	FILE* tmpFile;
	freopen_s(&tmpFile, "CONOUT$", "w", stdout);
#endif
					  
	MSG message;
	HACCEL acceleratorHandler;

	// Initialize global strings
	LoadString(instance, IDS_APP_TITLE, WindowTitle, MAX_LOADSTRING);
	LoadString(instance, IDC_AYEAYE, WindowClassName, MAX_LOADSTRING);
	MyRegisterClass(instance);

	// Perform application initialization:
	if (!InitializeInstance(instance, commandShow))
	{
		return FALSE;
	}

	acceleratorHandler = LoadAccelerators(instance, MAKEINTRESOURCE(IDC_AYEAYE));

	//Set start time in GameController
	if (GameController)
	{
		GameController->SetLastFrameTimeToCurrentTime();
	}

	// Main message loop:
	ZeroMemory(&message, sizeof(message));
							
	bool hasFocus = false;
	HWND window = NULL;
	while (message.message != WM_QUIT){
		window = message.hwnd;
		if (PeekMessage(&message, NULL, 0U, 0U, PM_REMOVE))
		{
			TranslateMessage(&message);
			DispatchMessage(&message);
		}
		//else 
		{
			// check if we have focus
			if (GetForegroundWindow() == window)
			{
				// Reset last time in the GameController 
				// if we lost focus
				if (!hasFocus && GameController)
					GameController->SetLastFrameTimeToCurrentTime();
				hasFocus = true;
			}
			else
			{
				hasFocus = false;
			}
			if (hasFocus && GameController)
			{
				GameController->GameLoop();
			}
		}
	}

#ifdef _DEBUG
	FreeConsole();
#endif
	return (int) message.wParam;
}

//  FUNCTION: MyRegisterClass()												   
//  PURPOSE: Registers the window class.
ATOM MyRegisterClass(HINSTANCE instance)
{
	WNDCLASSEX windowStructure;

	windowStructure.cbSize = sizeof(WNDCLASSEX);

	windowStructure.style = CS_HREDRAW | CS_VREDRAW;
	windowStructure.lpfnWndProc = WindowProcedure;
	windowStructure.cbClsExtra = 0;
	windowStructure.cbWndExtra = 0;
	windowStructure.hInstance = instance;
	windowStructure.hIcon = LoadIcon(instance, MAKEINTRESOURCE(IDI_AYEAYE));
	windowStructure.hCursor = LoadCursor(NULL, IDC_ARROW);
	windowStructure.hbrBackground = (HBRUSH) (COLOR_WINDOW + 1);
	windowStructure.lpszMenuName = MAKEINTRESOURCE(IDC_AYEAYE);
	windowStructure.lpszClassName = WindowClassName;
	windowStructure.hIconSm = LoadIcon(windowStructure.hInstance, MAKEINTRESOURCE(IDI_AYEAYE));

	return RegisterClassEx(&windowStructure);
}

//   FUNCTION: InitInstance(HINSTANCE, int)
//   PURPOSE: Saves instance handle and creates main window
//   COMMENTS:
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitializeInstance(HINSTANCE instanceHandler, int commandShow)
{
   HWND windowHandler;

   InstanceHandler = instanceHandler; // Store instance handle in our global variable

   windowHandler = CreateWindow(WindowClassName,
	   WindowTitle, WS_OVERLAPPEDWINDOW,
	   CW_USEDEFAULT, 0, CW_USEDEFAULT, 0,
	   NULL, NULL, instanceHandler, NULL);

   if (!windowHandler)
   {
      return FALSE;
   }
   
   Client_Rect;
   GetClientRect(windowHandler, &Client_Rect);
   windowSize.x = Client_Rect.right - Client_Rect.left;
   windowSize.y = Client_Rect.bottom + Client_Rect.left;

   GameController = new Controllers::GameController(windowHandler);
   GameEventHandler = new Controllers::GameEventHandler(GameController);

   ShowWindow(windowHandler, commandShow);
   UpdateWindow(windowHandler);

   //soundtest->Shutdown();

   return TRUE;
}

//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
LRESULT CALLBACK WindowProcedure(HWND window, UINT message, WPARAM windowParameter, LPARAM messageParameter)
{
	int windowId, windowEvent;
	PAINTSTRUCT paintStructure;
	HDC deviceContext;
	switch (message)
	{
	case WM_COMMAND:
		windowId = LOWORD(windowParameter);
		windowEvent = HIWORD(windowParameter);
		// Parse the menu selections:
		switch (windowId)
		{
		case IDM_ABOUT:
			DialogBox(InstanceHandler, MAKEINTRESOURCE(IDD_ABOUTBOX), window, About);
			break;
		case IDM_EXIT:
			DestroyWindow(window);
			break;
		default:
			return DefWindowProc(window, message, windowParameter, messageParameter);
		}
		break;
	case WM_PAINT:
			deviceContext = BeginPaint(window, &paintStructure);
			backBuffer = CreateCompatibleDC(deviceContext);
			memoryBitmap = CreateCompatibleBitmap(deviceContext, windowSize.x, windowSize.y);
			memoryOldBitmap = (HBITMAP) SelectObject(backBuffer, memoryBitmap);
			FillRect(backBuffer, &Client_Rect, (HBRUSH)(COLOR_WINDOW + 1));
			paintStructure.hdc = backBuffer;

			GameController->Paint(&paintStructure);

			paintStructure.hdc = deviceContext;
			BitBlt(deviceContext, 0, 0, windowSize.x, windowSize.y, backBuffer, 0, 0, SRCCOPY); 
			SelectObject(backBuffer, memoryOldBitmap);
			DeleteObject(memoryBitmap);
			DeleteDC(backBuffer);
			EndPaint(window, &paintStructure);	
		break;
	case WM_ERASEBKGND:
		return 1;
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_KEYDOWN:
		GameEventHandler->KeyDownEvent(windowParameter, messageParameter);
		break;
	case WM_KEYUP:
		GameEventHandler->KeyUpEvent(windowParameter, messageParameter);
		break;
	case WM_SIZE:
		GetClientRect(window, &Client_Rect);
		windowSize.x = Client_Rect.right - Client_Rect.left;
		windowSize.y = Client_Rect.bottom + Client_Rect.left;
		GameController->GetViewportController()->UpdateWindowSize(windowSize);
		break;
	default:
		return DefWindowProc(window, message, windowParameter, messageParameter);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM windowParameter, LPARAM messageParameter)
{
	UNREFERENCED_PARAMETER(messageParameter);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(windowParameter) == IDOK || LOWORD(windowParameter) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(windowParameter));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
