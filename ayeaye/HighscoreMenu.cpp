#include "stdafx.h"
#include "HighscoreMenu.h"
#include "ScoreController.h"

using namespace Models;

HighscoreMenu::HighscoreMenu(void)
{
	_hasBackground = true;
	_hasContent = true;
	SetHighscores();
	_height = 160;

	_backgroundImage = (HBITMAP) LoadImage(NULL, 
		"..\\GameData\\highscore_bg.bmp", 
		IMAGE_BITMAP, 
		0, 0, 
		LR_LOADFROMFILE);
}


HighscoreMenu::~HighscoreMenu(void)
{
}

void HighscoreMenu::SetHighscores()
{
	Controllers::ScoreController controller = Controllers::ScoreController();
	_highscores = controller.GetHighcores();

	for(int i = 0; i < 5; i++)
	{
		_content.push_back(std::to_string(i + 1) + ". " + std::to_string(_highscores[i]));
	}
}