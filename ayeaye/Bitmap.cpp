#include "stdafx.h"
#include "Bitmap.h"

#include <vector>

using namespace Models;

Bitmap::Bitmap(HWND window, std::istream& is)
{
	// copy stream to vector
	std::vector<char> buffer;
	while (is.good())
	{
		buffer.push_back(is.get());
	}

	// construct a win32 bitmap
	HDC hdc = GetDC(window);
	HDC memDc = CreateCompatibleDC(hdc);
	char * rawData = &(buffer[0]);
	BITMAPFILEHEADER *fileHeader = (BITMAPFILEHEADER*)rawData;
	BITMAPINFO *bitmapInfo = (BITMAPINFO*)(rawData + sizeof(BITMAPFILEHEADER));
	_width = bitmapInfo->bmiHeader.biWidth;
	_height = bitmapInfo->bmiHeader.biHeight;
	void * bitmapData = rawData + fileHeader->bfOffBits;
	_bitmap = CreateCompatibleBitmap(hdc, _width, _height);
	SelectObject(memDc, _bitmap);
	SetDIBitsToDevice(memDc,
		0,
		0,
		_width,
		_height,
		0,
		0,
		0,
		_height,
		bitmapData,
		bitmapInfo,
		DIB_RGB_COLORS);
	DeleteDC(memDc);
	ReleaseDC(window, hdc);
}

Bitmap::~Bitmap()
{
			
}

int Bitmap::GetWidth() const
{
	return _width;
}

int Bitmap::GetHeight() const
{
	return _height;
}

void Bitmap::Render(HDC hdc, 
	int srcX, int srcY, 
	int dstX, int dstY, 
	int srcW, int srcH) const
{
	HDC memDc = CreateCompatibleDC(hdc);

	SelectObject(memDc, _bitmap);

	GdiTransparentBlt(hdc,
		dstX,
		dstY,
		srcW,
		srcH,
		memDc,
		srcX,
		srcY,
		srcW,
		srcH,
		RGB(255, 0, 255));

	DeleteDC(memDc);
}

BitmapStore::BitmapStore()
{
}

BitmapStore* BitmapStore::GetInstance()
{
	static BitmapStore instance;
	return &instance;
}

Bitmap* BitmapStore::LoadBitmapFromInput(std::string name, HWND hWnd, std::istream& is)
{
	Bitmap * bmp = new Bitmap(hWnd, is);
	_loadedBitmaps[name] = bmp;
	return bmp;
}

bool BitmapStore::IsLoaded(std::string name)
{
	return _loadedBitmaps.count(name) > 0;
}

Bitmap* BitmapStore::GetBitmap(std::string name)
{
	return _loadedBitmaps[name];
}

void BitmapStore::EmptyStore()
{
	_loadedBitmaps.clear();
}

BitmapStore::~BitmapStore()
{
	EmptyStore();
}

