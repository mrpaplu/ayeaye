#pragma once

#include <vector>
#include "Level.h"
#include "Menu.h"

namespace Models 
{
	class MainMenu : public Menu
	{
	public:
		MainMenu(void);
		~MainMenu(void);
		void SetMenuItems();
	};
}
