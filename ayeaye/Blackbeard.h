#pragma once

#include "MovingEnemy.h"
#include "BlackbeardAnimator.h"
#include "BlackbeardController.h"

namespace Models
{
	class Blackbeard : public MovingEnemy
	{
	public:
		Blackbeard(Point<float>);
		~Blackbeard(void);
		static Blackbeard* Blackbeard::MakeGameObject(const Loaders::GameObjectProperties& props);
	};
}
