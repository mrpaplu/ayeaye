#pragma once

#include <string>

namespace Utils
{
	std::string Trim(std::string& s);
	std::istream& getline(std::istream& i, std::string& line);
	bool CompareIgnoreCaseStrings(const std::string first, const std::string second);
}