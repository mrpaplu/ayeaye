#pragma once
#include "Sprite.h"

namespace Models
{
	class Background
	{
	private:
		Sprite* _sprite[2];
		int _maxY;
	public:
		Background(void);
		~Background(void);
		void SetSprite(int, Sprite*);
		Sprite* GetSprite(int);
		void SetMaxY(int);
		int GetMaxY();
	};
}
