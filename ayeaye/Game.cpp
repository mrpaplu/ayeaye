#include "stdafx.h"
#include "Game.h"

using namespace Models;

Game::Game()
{
	_gameState = GameState::Started;
}

Game::~Game()
{
}

GameState Game::GetGameState()
{
	return _gameState;
}

void Game::SetGameState(GameState state)
{
	_gameState = state;
}