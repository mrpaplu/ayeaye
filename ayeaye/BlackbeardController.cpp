#include "stdafx.h"
#include "BlackbeardController.h"
#include "Blackbeard.h"
#include <iostream>

using namespace Controllers;

BlackbeardController::BlackbeardController(void)
{
}


BlackbeardController::~BlackbeardController(void)
{
}

Models::Blackbeard* BlackbeardController::GetModel()
{
	return _model;
}

void BlackbeardController::Update(float delta)
{
	// physics
	if (GetModel()->IsFalling())
	{
		FallDown(delta);
		Point<float> v = _model->GetVelocity();
		if (v.y > 1000)
		{
			v.y = 1000;
			_model->SetVelocity(v);
		}
	}

	GetModel()->SetIsFalling(true);

	Point<float> p = _model->GetPosition();
	p.x += _model->GetVelocity().x * delta;
	p.y += _model->GetVelocity().y * delta;
	_model->SetPosition(p);
}

void BlackbeardController::SetModel(Models::Blackbeard* bb)
{
	_model = bb;
}

void BlackbeardController::FallDown(float delta)
{
	if (!_model && !_model->IsFalling())
		return;
	Point<float> p = { 0, 250*delta };
	_model->GetMovement()->AlterMovement(p);
}

void BlackbeardController::OnCollision(Models::GameObject* other, Models::CollisionDirection d)
{
	bool didSwapDirection = false;

	if (other->GetType() != "Rum" && other->GetType() != "Mushroom" && other->GetType() !=  "bomb")
	{
		if (!_model->CanMoveLeft() || !_model->CanMoveRight())
		{
			didSwapDirection = true;
			Point<float> p = GetModel()->GetVelocity();
			p.x *= -1;
			_model->SetVelocity(p);
		}

		switch (d)
		{
		case Models::CollisionDirection::Top:
			GetModel()->SetPosY(other->GetPosY() - GetModel()->GetHeight());
			GetModel()->SetIsFalling(false);
			break;
		case Models::CollisionDirection::Bottom:
		{
			GetModel()->SetPosY(other->GetPosY() + other->GetHeight());
			Point<float> v = { GetModel()->GetVelocity().x, 0 };
			GetModel()->SetVelocity(v);
		}
			break;
		case Models::CollisionDirection::Left:
		{
			if (!didSwapDirection)
			{
				Point<float> p = GetModel()->GetVelocity();
				p.x *= -1;
				_model->SetVelocity(p);
			}
		}
			break;
		case Models::CollisionDirection::Right:
		{
			if (!didSwapDirection)
			{
				Point<float> p = GetModel()->GetVelocity();
				p.x *= -1;
				_model->SetVelocity(p);
			}
		}
			break;
		default:
			break;
		}
	}
}


void BlackbeardController::OnOutOfLevel(Models::CollisionDirection dir, bool outOfLevel, RECT levelBounds)
{	
}