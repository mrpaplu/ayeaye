#include "stdafx.h"

#include <string>
#include <fstream>
#include <algorithm>
#include "LevelLoaderError.h"  
#include "GameObjectLoader.h"
#include "Utils.h"
#include "ZipArchive.h"
#include "GameObjectFactory.h"
#include "Bitmap.h"
#include "SpriteMapLoader.h"

using namespace Loaders;

GameObjectLoader::GameObjectLoader()
{
}

GameObjectLoader::~GameObjectLoader()
{
}

Models::GameObject* GameObjectLoader::Load(HWND window, 
	std::istream& input, 
	const std::vector<Models::Sprite*>& lvlSpriteMap)
{
	std::string line;
	GameObjectProperties props;
	while (Utils::getline(input, line))
	{
		std::string key = Utils::Trim(line.substr(0, line.find(':')));
		std::string value = Utils::Trim(line.substr(line.find(':') + 1));
		if (key == "WIDTH")
			props.width = atoi(value.c_str());
		else if (key == "HEIGHT")
			props.height = atoi(value.c_str());
		else if (key == "POSY")
			props.posy = atoi(value.c_str());
		else if (key == "POSX")
			props.posx = atoi(value.c_str());
		else if (key == "END" && value == "GAMEOBJECT")
			return Loaders::GameObjectFactory::Create(props);
		else if (key == "TYPE")
			SetFromType(window, props, value, lvlSpriteMap);
		else
			throw LevelLoad_error();
	}
	throw LevelLoad_error();
}

static Models::Sprite* GetNamedSprite(const std::string& typeName,
	const std::vector<Models::Sprite*>& sprites)
{
	std::vector<Models::Sprite*>::const_iterator it;
	for (it = sprites.begin(); it != sprites.end(); it++)
	{
		if ((*it)->GetName() == typeName)
		{
			return *it;
		}  			
	}
	return NULL;
}

void Loaders::GameObjectLoader::SetFromType(HWND window,
	GameObjectProperties& props,
	const std::string& typeName,
	const std::vector<Models::Sprite*>& lvlSpriteMap)
{
	Models::BitmapStore * bmpStore = Models::BitmapStore::GetInstance();
	const std::string goBasePath = "..\\GameData\\GameObjects";
	std::string filePath = goBasePath + "\\" + typeName + ".arg";
	std::string goFile = typeName + ".txt";
	Loaders::ZipArchive zip(filePath);
	std::istream file(zip.Open(goFile));
	std::string line;
													 
	Utils::getline(file, line);
	if (Utils::Trim(line) != "BEGIN:GAMEOBJECT")
		throw LevelLoad_error();

	bool hasOwnSpriteMap = false;
	if (zip.FileExists("spritemap.bmp"))
	{
		hasOwnSpriteMap = true;
	}			

	Models::Bitmap * bmp = NULL;
	if (hasOwnSpriteMap)
	{
		if (!bmpStore->IsLoaded(typeName))
		{
			std::istream bmpIs(zip.Open("spritemap.bmp"));
			bmp = bmpStore->LoadBitmapFromInput(typeName, window, bmpIs);
		}
		else
		{
			bmp = bmpStore->GetBitmap(typeName);
		}
	}

	props.type = typeName;
	while (Utils::getline(file, line))
	{
		std::string key = Utils::Trim(line.substr(0, line.find(':')));
		std::string value = Utils::Trim(line.substr(line.find(':') + 1));
		if (key == "WIDTH")
			props.width = atoi(value.c_str());
		else if (key == "HEIGHT")
			props.height = atoi(value.c_str());
		else if (key == "POSY")
			props.posy = atoi(value.c_str());
		else if (key == "POSX")
			props.posx = atoi(value.c_str());
		else if (hasOwnSpriteMap && key == "BEGIN" && value == "SPRITEMAP")
			props.sprites = SpriteMapLoader::LoadSpriteMap(file, bmp);
		else if (key == "END" && value == "GAMEOBJECT")
		{
			if (!hasOwnSpriteMap)
			{
				props.sprites.clear();
				Models::Sprite* s = GetNamedSprite(typeName, lvlSpriteMap);
				if (s != NULL)
					props.sprites.push_back(s);
			}
			return;
		}
		else if (key == "TYPE"){} //ignore
		else if (key == "NAME"){}//ignore
		else if (key == "COLOR") {}//ignore 
		else
			throw LevelLoad_error();
	}
	throw LevelLoad_error();
}