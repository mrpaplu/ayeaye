#pragma once

#include "Menu.h"

namespace Models 
{
	class HighscoreMenu : public Menu
	{
	public:
		HighscoreMenu(void);
		~HighscoreMenu(void);
		void SetHighscores();
		void SetMenuItems()
		{
			_items.push_back("Back");
		}
	private:
		int* _highscores;
	};
}

