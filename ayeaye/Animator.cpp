#include "stdafx.h"
#include "Animator.h"

using namespace Controllers;

Animator::Animator(std::vector<Models::Sprite*> sprites, Models::GameObject* ourObject)
{
	_sprites = sprites;
	_ourObject = ourObject;
	_currentSprite = sprites[0];
}

Animator::~Animator()
{				 
}

int Animator::GetWidth() const
{
	return _currentSprite->GetWidth();
}

int Animator::GetHeight() const
{
	return _currentSprite->GetHeight();
}

void Animator::Render(HDC hdc, int xDest, int yDest) const
{
	_currentSprite->Render(hdc, xDest, yDest);
}