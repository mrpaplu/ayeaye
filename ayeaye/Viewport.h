#pragma once
#include "Point.h"

namespace Models
{
	class Viewport
	{
	public:
		Viewport(void);
		Viewport(Point<int>);
		Viewport(Point<int>, Point<int>);
		void SetPosition(Point<int>);
		void SetSize(Point<int>);
		Point<int> GetPosition();
		Point<int> GetSize();
		~Viewport(void);
	private:
		Point<int> _position;
		Point<int> _size;
	};
}
