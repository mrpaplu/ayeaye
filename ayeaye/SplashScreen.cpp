#include "stdafx.h"
#include "SplashScreen.h"
#include <string>

using namespace Models;

SplashScreen::SplashScreen()
{
}


SplashScreen::~SplashScreen()
{
}

void SplashScreen::SetActive(bool active)
{
	_isActive = active;
}

bool SplashScreen::IsActive()
{
	return _isActive;
}

std::vector<std::string> SplashScreen::GetLines()
{
	return _lines;
}

void SplashScreen::GetLevelSplash(std::string levelName)
{
	std::string beach = "beach";
	std::string space = "space";
	std::string jungle = "jungle";
	std::string ocean = "ocean";
	std::string ship = "ship";
	std::string town = "town";

	if(levelName == beach)
	{
		SplashScreenBeach();
	}
	else if(levelName == space)
	{
		SplashScreenSpace();
	}
	else if(levelName == jungle)
	{
		SplashScreenJungle();
	}
	else if(levelName == ocean)
	{
		SplashScreenOcean();
	}
	else if(levelName == ship)
	{
		SplashScreenShip();
	}
	else if (levelName == town)
	{
		SplashScreenTown();
	}
	else 
	{
		SplashScreenDefault();
	}
}

void SplashScreen::SplashScreenDefault()
{
	_lines.clear();
	_lines.push_back("No splash yet");
}

void SplashScreen::SplashScreenTextStarted()
{
	_lines.clear();
	_lines.push_back("Waking up on a beach, Aye finds himself");
	_lines.push_back("without a bottle of rum in his hands. In this");
	_lines.push_back("adventure our hero Aye goes on an epic quest ");
	_lines.push_back("to find the rum he so desperately desires.");
}

void SplashScreen::SplashScreenTextEnded()
{
	_lines.clear();
	_lines.push_back("Thanks for playing AyeAye");
	_lines.push_back("We hope you've enjoyed your time and had some fun!");
}

void SplashScreen::SplashScreenBeach()
{
	_lines.clear();
	_lines.push_back("It seems I�m shipwrecked.");
	_lines.push_back("But wait, where�s my rum?");
}

void SplashScreen::SplashScreenJungle()
{
	_lines.clear();
	_lines.push_back("Welcome to the jungle, baby!");
	_lines.push_back("We got rum and games!");
}

void SplashScreen::SplashScreenTown()
{
	_lines.clear();
	_lines.push_back("Hmmmm... I'm out of rum let's head to town");
	_lines.push_back("and see if they have something stronger than rum!");
}

void SplashScreen::SplashScreenOcean()
{
	_lines.clear();
	_lines.push_back("Nothing but sea,");
	_lines.push_back("not even a bottle afloat.");
}

void SplashScreen::SplashScreenShip()
{
	_lines.clear();
	_lines.push_back("Blackbeard is trying to steal my rum.");
	_lines.push_back("Better stop him!");
}

void SplashScreen::SplashScreenSpace()
{
	_lines.clear();
	_lines.push_back("Holy space cows!");
	_lines.push_back("They�ve got rum in space!");
}

void SplashScreen::Credits()
{
	_lines.clear();
	_lines.push_back("Written by:");
	_lines.push_back("Arjan Speiard, Justin Peuijn, Wouter Brunekreeft,");
	_lines.push_back("Carlos Bruckner, Daan  Flobbe, Kay van Bree");
}

void SplashScreen::Lose()
{
	_lines.clear();
	_lines.push_back("\"Arghh, more rum for us!\"");
	_lines.push_back("- Blackbeard");
}