#include "stdafx.h"
#include "DebugInfoView.h"
#include <string>

using namespace Views;

DebugInfoView::DebugInfoView()
{

}

DebugInfoView::DebugInfoView(Models::DebugInfo* debugInfo)
{
	_debugInfo = debugInfo;
}

DebugInfoView::~DebugInfoView()
{

}

void DebugInfoView::PaintGeneralInfo(PAINTSTRUCT* paintstruct, Controllers::ViewportController& viewportController)
{
	if (_debugInfo->DebugOn)
	{
		ShowAllDebugInfo(paintstruct, viewportController);
	}
}

void DebugInfoView::PaintObjectInfo(PAINTSTRUCT* paintstruct, Controllers::ViewportController& viewportController, const Models::GameObject* gameObject)
{
	if (_debugInfo->DebugOn)
	{
		ShowObjectBorders(paintstruct, viewportController, gameObject);
		ShowLevelBorder(paintstruct);
	}
}

void DebugInfoView::ShowAllDebugInfo(PAINTSTRUCT* paintstruct, Controllers::ViewportController& viewportController)
{
	ShowFPS(paintstruct, viewportController);
	ShowViewportInfo(paintstruct, viewportController);
}

void DebugInfoView::ShowFPS(PAINTSTRUCT* paintstruct,  Controllers::ViewportController& viewportController)
{
	SetBkMode(paintstruct->hdc, TRANSPARENT);
	SetTextColor(paintstruct->hdc, RGB(0,0,0));

	std::string fpsString = std::to_string(_debugInfo->GetFPS())+" FPS";

	Point<float> position;
	position.x = viewportController.Right() - 100;
	position.y = viewportController.Top() + 10;
	position = viewportController.TranslateCoordinates(position);

	TextOut(paintstruct->hdc, position.x, position.y, fpsString.c_str(), fpsString.length());
}

void DebugInfoView::ShowObjectBorders(PAINTSTRUCT* paintstruct, Controllers::ViewportController& viewportController, const Models::GameObject* gameObject) 
{
	SetTextColor(paintstruct->hdc, RGB(0,0,0));
	SelectObject(paintstruct->hdc, GetStockObject(NULL_BRUSH));
	int width = gameObject->GetWidth();
	int height = gameObject->GetHeight();
	Point<int> startPoint;
	startPoint.x = gameObject->GetPosition().x;
	startPoint.y = gameObject->GetPosition().y;
	Point<int> endPoint;
	endPoint.x = gameObject->GetPosition().x + width;
	endPoint.y = gameObject->GetPosition().y + height;

	Point<int> screenStartPoint = viewportController.TranslateCoordinates(startPoint);
	Point<int> screenEndPoint = viewportController.TranslateCoordinates(endPoint);
	
	Rectangle(paintstruct->hdc, screenStartPoint.x, screenStartPoint.y, screenEndPoint.x, screenEndPoint.y);
	TextOut(paintstruct->hdc, screenStartPoint.x, screenStartPoint.y -35, std::to_string(startPoint.x).c_str(), std::to_string(startPoint.x).length());
	TextOut(paintstruct->hdc, screenStartPoint.x, screenStartPoint.y-15, std::to_string(startPoint.y).c_str(), std::to_string(startPoint.y).length());
}

void DebugInfoView::ShowViewportInfo(PAINTSTRUCT* paintstruct, Controllers::ViewportController& viewportController)
{
	SetTextColor(paintstruct->hdc, RGB(0,0,0));
	SetBkMode(paintstruct->hdc, TRANSPARENT);

	std::string fpsString = std::to_string(_debugInfo->GetViewportPosition().x)+"X ";
	fpsString += std::to_string(_debugInfo->GetViewportPosition().y)+"Y ";
	
	Point<int> position;
	position.x = viewportController.Right() - 100;
	position.y = viewportController.Top() + 30;
	position = viewportController.TranslateCoordinates(position);

	TextOut(paintstruct->hdc, position.x, position.y, fpsString.c_str(), fpsString.length());
}

void DebugInfoView::ShowLevelBorder(PAINTSTRUCT* paintstruct)
{
	SelectObject(paintstruct->hdc, GetStockObject(NULL_BRUSH));
	Rectangle(paintstruct->hdc, 0, 0, paintstruct->rcPaint.right, paintstruct->rcPaint.bottom);
}