#pragma once

#include<iosfwd>
#include<string>

namespace Models
{
	class Bitmap;
	class Sprite
	{
	public:
		Sprite();
		Sprite(RECT region, Bitmap* bitmap, const std::string name = "");
		~Sprite();
		virtual void Render(HDC hdc, int xDest, int yDest) const;	  
		virtual void UpdateSprite(float delta){};
		virtual int GetWidth() const;
		virtual int GetHeight() const;
		virtual const std::string& GetName() const;
	private:
		int _width;
		int _height;
		Bitmap * _bitmap;
		RECT _region;
		std::string _name;
	};
}