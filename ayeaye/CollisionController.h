#pragma once

#include <vector>
#include <map>
#include "PlayerController.h"
#include "Level.h"

namespace Controllers {
	class CollisionController
	{
	public:
		CollisionController();
		CollisionController(Models::Level*);
		~CollisionController();
		void CheckCollisions(float delta);
	private:
		Models::Level* _level;
	};
}