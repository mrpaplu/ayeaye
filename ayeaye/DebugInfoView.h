#pragma once

#include "DebugInfo.h"
#include "GameObject.h"
#include "ViewportController.h"

namespace Views
{
	class DebugInfoView
	{
	public:
		DebugInfoView();
		DebugInfoView(Models::DebugInfo*);
		void PaintObjectInfo(PAINTSTRUCT*,  Controllers::ViewportController&, const Models::GameObject*);
		void PaintGeneralInfo(PAINTSTRUCT*,  Controllers::ViewportController&);
		~DebugInfoView();
	private:
		Models::DebugInfo* _debugInfo;
		void DebugInfoView::ShowAllDebugInfo(PAINTSTRUCT*, Controllers::ViewportController&);
		void DebugInfoView::ShowFPS(PAINTSTRUCT*, Controllers::ViewportController&);
		void ShowObjectBorders(PAINTSTRUCT*, Controllers::ViewportController&, const Models::GameObject*);
		void ShowViewportInfo(PAINTSTRUCT* paintstruct,  Controllers::ViewportController&);
		void DebugInfoView::ShowLevelBorder(PAINTSTRUCT* paintstruct);
	};
}