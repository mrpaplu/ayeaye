#include "stdafx.h"
#include "ScoreView.h"
#include <string>

using namespace Views;

ScoreView::ScoreView(void)
{
	_fontHeight = 42;
	_heartImage = (HBITMAP) LoadImage(NULL, 
		"..\\GameData\\heart2.bmp", 
		IMAGE_BITMAP, 
		0, 0, 
		LR_LOADFROMFILE);
	_heartImageBig = (HBITMAP) LoadImage(NULL, 
		"..\\GameData\\heart3.bmp", 
		IMAGE_BITMAP, 
		0, 0, 
		LR_LOADFROMFILE);
	_isBig = false;
	_selected = _heartImage;
	_frametime = 0;
}


ScoreView::~ScoreView(void)
{
}

void ScoreView::Paint(PAINTSTRUCT* paintstruct,  Controllers::ViewportController& viewportController, int score, int lives, int delta)
{
	PaintScore(paintstruct, viewportController, score);
	PaintLives(paintstruct, viewportController, lives, delta);
}

void ScoreView::PaintLives(PAINTSTRUCT* paintstruct,  Controllers::ViewportController& viewportController, int lives, int delta)
{
	_frametime += delta;

	if (lives == 1 && _frametime > 150)
	{
		_frametime = 0;
		_isBig = !_isBig;

		if (_isBig)
		{
			_selected = _heartImageBig;
		}
		else
		{
			_selected = _heartImage;
		}
	}

	HDC bitmapContext = CreateCompatibleDC(paintstruct->hdc);
	for(int i = 0; i < lives; i++)
	{
		SelectObject(bitmapContext, _selected);

		Point<float> position;
		position.x = viewportController.Left() + i * 30 + 25;
		position.y = viewportController.Top() + 25;
		position = viewportController.TranslateCoordinates(position);
		GdiTransparentBlt(paintstruct->hdc,
			position.x,
			position.y,
			26,
			24,
			bitmapContext,
			paintstruct->rcPaint.top,
			paintstruct->rcPaint.left,
			26,
			24,
			RGB(255, 0, 255));
	}
	DeleteDC(bitmapContext);
}

void ScoreView::PaintScore(PAINTSTRUCT* paintstruct,  Controllers::ViewportController& viewportController, int score)
{
	// Set font
	SetBkMode(paintstruct->hdc, TRANSPARENT);
	AddFontResource("..\\GameData\\8bitoperator.ttf");
	HFONT font = CreateFont(_fontHeight, 0, 0, 0, 0, FALSE, FALSE, FALSE, 
		DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, 
		DEFAULT_QUALITY, FF_DONTCARE, TEXT("8bitoperator"));
	HGDIOBJ oldFont = GetCurrentObject(paintstruct->hdc, OBJ_FONT);
	SetTextColor(paintstruct->hdc, RGB(255,255,255));
	SelectObject(paintstruct->hdc, font);

	// Draw score
	std::string scoreString = std::to_string(score);

	Point<float> position;
	position.x = ((viewportController.Right() - viewportController.Left()) / 2) + viewportController.Left() - (scoreString.length() / 2 * (_fontHeight / 3));
	position.y = viewportController.Top() + 10;
	position = viewportController.TranslateCoordinates(position);

	TextOut(paintstruct->hdc, position.x, position.y, scoreString.c_str(), scoreString.length());

	// Remove stuff
	RemoveFontResource("..\\GameData\\8bitoperator.ttf");
	DeleteObject(font);
	SelectObject(paintstruct->hdc, oldFont);
	DeleteObject(oldFont);
}