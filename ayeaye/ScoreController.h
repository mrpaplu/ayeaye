#pragma once
#include "ScoreView.h"
namespace Controllers
{
	class ScoreController
	{
	public:
		ScoreController(void);
		~ScoreController(void);
		void LoadHighscores();
		int* GetHighcores();
		void SaveHighscores();
		void SetHighscore(int score);
		void ResetHighscores();
		Views::ScoreView* GetView();
	private:
		int* _highscores;
		Views::ScoreView* _scoreView;
	};
}