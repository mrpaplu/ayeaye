#pragma once
#include "Point.h"
#include "GameObject.h"
#include "Movement.h"

namespace Models
{
	class MovingGameObject : public GameObject
	{
	public:
		MovingGameObject(void);
		~MovingGameObject(void);
		MovingGameObject(Point<float>);
		Movement* GetMovement();
		virtual Point<float> GetVelocity();
		virtual Point<float> GetDefaultVelocity() const;
		virtual void SetVelocity(Point<float> p);
		virtual bool IsMoveable();
		bool IsFalling();
		void SetIsFalling(bool);
		void AlterMovement(Point<float>&);	   
	protected:
		bool _isFalling;
		Movement _movement;
		Point<float> _defaultVelocity;
	};
}
