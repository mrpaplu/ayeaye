#include "stdafx.h"
#include "Rum.h"

using namespace Models;

Rum::Rum(void)
{
}


Rum::~Rum(void)
{
}

Rum* Rum::MakeGameObject(const Loaders::GameObjectProperties& props)
{
	Models::Rum * go = new Models::Rum();
	Controllers::Animator * animator = new Controllers::RumAnimator(props.sprites, go);
	go->SetWidth(props.width);
	go->SetHeight(props.height);
	go->SetPosX(props.posx);
	go->SetPosY(props.posy);
	go->SetType(props.type);
	go->SetSprite(animator);
	return go;
}
