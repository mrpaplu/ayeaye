#pragma once

#include "GameObjectLoader.h"

#include "Utils.h"
#include "GameObject.h"

namespace Models
{
	class GameObject;
}

namespace Loaders
{
	class GameObjectFactory
	{
	public:
		static Models::GameObject* Create(const GameObjectProperties&);
	};
}