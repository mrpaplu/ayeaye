#include "stdafx.h"
#include "GameEventHandler.h"
#include "GameController.h"

using namespace Controllers;

GameEventHandler::GameEventHandler(GameController* g)
{
	_gameController = g;
}

GameEventHandler::~GameEventHandler()
{
}

void GameEventHandler::KeyDownEvent(WPARAM windowParameter, LPARAM messageParameter)
{
	//player
	Controllers::PlayerController *playerController = _gameController->GetPlayerController();
	if (playerController != NULL)
	{
		switch (windowParameter)
		{
		case (VK_LEFT) :
		case ('A') :
				   playerController->InputLeft();
			break;

		case (VK_RIGHT) :
		case ('D') :
				   playerController->InputRight();
			break;
		case (VK_UP) :
		case ('W') :
				   playerController->InputTop();
			break;
		case (VK_DOWN) :
		case ('S') :
				   //_playerController->InputBottom();
				   break;
		}
	}
	
	// debug
	Controllers::DebugController *debugController = _gameController->GetDebugController();
	if (debugController != NULL)
	{
		switch (windowParameter)
		{
		case (VK_F11) :
			debugController->ToggleDebugMode();
			break;
		}
	}

	//SplashScreen
	Controllers::SplashScreenController *splashScreenController = _gameController->GetSplashScreenController();
	if (splashScreenController != NULL)
	{
		Models::GameState state = _gameController->GetGame()->GetGameState();
		if(state == Models::GameState::Loading)
		{
			return;
		}
		else if (state == Models::GameState::Ended)
		{
			splashScreenController->TurnSplashScreenOff();
			_gameController->GetGame()->SetGameState(Models::GameState::Exited);
			return;
		}
		else if (state == Models::GameState::Started)
		{
			splashScreenController->TurnSplashScreenOff();
			return;
		}
		else if((state == Models::GameState::Lost || state == Models::GameState::Credits)
			&& (windowParameter == VK_RETURN
			|| windowParameter == VK_SPACE
			|| windowParameter == VK_ESCAPE))
		{
			splashScreenController->TurnSplashScreenOff();
			_gameController->GetMenuController()->SetMainMenu();
			return;
		}
	}

	// menu
	Controllers::MenuController *menuController = _gameController->GetMenuController();
	if (menuController != NULL)
	{
		switch (windowParameter)
		{
		case (VK_UP) :
		case ('W') :
			if (menuController->GetMenu()->IsActive())
			{
				menuController->GetMenu()->SelectPrevious();
			}
			break;
		case (VK_DOWN) :
		case ('S') :
			if (menuController->GetMenu()->IsActive())
			{
				menuController->GetMenu()->SelectNext();
			}
			break;
		case (VK_ESCAPE) :
			menuController->ToggleMenu();
			break;
		case (VK_RETURN) :
		case (VK_SPACE) :
			if (menuController->GetMenu()->IsActive())
			{
				menuController->SelectCurrentItem(_gameController);
			}
			break;
		}
	}

}

void GameEventHandler::KeyUpEvent(WPARAM windowParameter, LPARAM messageParameter)
{						
	//player
	Controllers::PlayerController *playerController = _gameController->GetPlayerController();
	if (playerController != NULL)
	{
		switch (windowParameter)
		{
		case ('A') :
		case (VK_LEFT) :
					   playerController->StopLeft();
			break;
		case (VK_RIGHT) :
		case ('D') :
				   playerController->StopRight();
			break;
		case ('W') :
		case (VK_UP) :
					 playerController->StopY();
			break;
		case ('S') :
		case (VK_DOWN) :
					   playerController->StopY();
			break;
		}
	}
}