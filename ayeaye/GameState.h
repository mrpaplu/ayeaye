#pragma once 
namespace Models {
	enum class GameState
	{
		InMainMenu,
		Paused,
		Playing,
		Ended,
		Won,
		Started,
		Exited,
		Lost,
		Loading,
		NewGame,
		LoadingFirstLevel,
		Credits
	};
}