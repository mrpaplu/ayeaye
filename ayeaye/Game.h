#pragma once

#include "GameState.h"

namespace Models
{
	class Game
	{
	public:
		Game();
		~Game();
		GameState GetGameState();
		void SetGameState(GameState);
	private:
		GameState _gameState;
	};
}