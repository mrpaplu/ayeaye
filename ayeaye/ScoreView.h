#pragma once
#include "ViewportController.h"

namespace Views
{
	class ScoreView
	{
	public:
		ScoreView(void);
		~ScoreView(void);
		void Paint(PAINTSTRUCT*,  Controllers::ViewportController&, int, int, int);
		void PaintLives(PAINTSTRUCT*, Controllers::ViewportController&, int, int);
		void PaintScore(PAINTSTRUCT*, Controllers::ViewportController&, int);
	private:
		int _fontHeight;
		HBITMAP _heartImage;
		HBITMAP _heartImageBig;
		HBITMAP _selected;
		bool _isBig;
		int _frametime;
	};
}