#include "stdafx.h"
#include "MenuController.h"
#include "MainMenu.h"
#include "GameController.h"
#include "HighscoreMenu.h"

using namespace Controllers;

MenuController::MenuController(void)
: _menu(NULL), _menuView(NULL)
{
	SetMainMenu();
}

MenuController::~MenuController(void)
{
	Cleanup();
}

void MenuController::SetActive(bool b)
{
	if (_menu)
		_menu->SetActive(b);
}

void MenuController::SetHighscoreMenu()
{
	Cleanup();
	_menu = new Models::HighscoreMenu();
	_menu->SetMenuItems();
	_menu->SetActive(true);
	_menuView = new Views::MenuView(_menu);
}

void MenuController::SetMainMenu()
{
	Cleanup();
	_menu = new Models::MainMenu();
	_menu->SetMenuItems();
	_menuView = new Views::MenuView(_menu);
}

void MenuController::SetIngameMenu()
{
	Cleanup();
	_menu = new Models::IngameMenu();
	_menu->SetMenuItems();
	_menuView = new Views::MenuView(_menu);
}

void MenuController::ToggleMenu()
{
	if(_menu->IsToggleable())
	{
		_menu->SetActive(!_menu->IsActive());
	}
}

void MenuController::PlayMusic()
{
	Models::Sound* music = GetMusic();
	if(!music->IsPlaying())
	{
		_soundController->GetView()->PlayLoop(music);
	}
}

void MenuController::StopMusic()
{
	Models::Sound* music = GetMusic();
	if(music->IsPlaying())
	{
		_soundController->GetView()->Stop(music);
	}
}

Models::Menu* MenuController::GetMenu()
{
	return _menu;
}

void MenuController::SelectCurrentItem(GameController* g)
{
	std::string selectedString = _menu->GetMenuItems()->at(_menu->GetSelectedIndex());

	if(selectedString == "Resume")
	{
		ToggleMenu();
	}
	else if(selectedString == "New game")
	{
		g->SetCurrentLevel(0);
		SetIngameMenu();
		g->GetGame()->SetGameState(Models::GameState::NewGame);
		SetActive(false);
		StopMusic();
	}
	else if(selectedString == "Main menu")
	{
		g->QuitLevel();
		SetMainMenu();
		g->StopLevelMusic();
		PlayMusic();
	}
	else if(selectedString == "Save game")
	{
		g->SaveGame();
		ToggleMenu();
	}
	else if(selectedString == "Load game")
	{
		std::string* levelParams = g->GetSavedGame();
		SetIngameMenu();
		std::string score = levelParams[1];
		if(score == "")
			g->LoadLevel(levelParams[0], 0);
		else
			g->LoadLevel(levelParams[0], stoi(score));
		StopMusic();
	}
	else if(selectedString == "Highscores")
	{
		SetHighscoreMenu();
	}
	else if(selectedString == "Back")
	{
		SetMainMenu();
	}
	else if(selectedString == "Quit")
	{
		g->StopLevelMusic();
		PlayMusic();
		g->GetGame()->SetGameState(Models::GameState::Ended);
	}
}

void MenuController::Paint(HWND window, PAINTSTRUCT* ps)
{
	if(_menu->IsActive())
	{
		_menuView->Paint(window, ps);
	}
}

void MenuController::Cleanup()
{
	if (_menu)
	{
		delete _menu;
		_menu = NULL;
	}
	if (_menuView)
	{
		delete _menuView;
		_menuView = NULL;
	}
}

void MenuController::SetSoundController(SoundController* sc)
{
	_soundController = sc;
}

Models::Sound* MenuController::GetMusic()
{
	return &_menuMusic;
}