#pragma once

#include "MovingGameObject.h"
#include "Movement.h"
#include "Keyboard.h"
#include "Sound.h"
#include "GameObjectProperties.h"
#include "PlayerController.h"
#include "PlayerAnimator.h"

namespace Models
{
	class Player : public MovingGameObject
	{
	public:
		Player(Point<float>);
		~Player();
		Keyboard* GetKeyboard();
		static Player* Player::MakeGameObject(const Loaders::GameObjectProperties& props);
		int GetScore();
		void SetScore(int);
		void AddScore(int);
		void Respawn();
		void Win();
		bool HasWon();
		Sound* GetJumpSound();
		Sound* GetDieSound();
		void Die();
		bool HasLost();
		int GetLives();
		void SetLives(int);
	private:
		Keyboard _keyboard;
		Sound _jumpSound;
		Sound _dieSound;
		int _score;
		bool _hasWon;
		Point<float> _spawnPoint;
		int _lives;
		bool _hasLost;		 
	};
}