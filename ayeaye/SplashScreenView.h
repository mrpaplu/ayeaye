#pragma once

#include <vector>
#include "SplashScreen.h"

namespace Views {
	class SplashScreenView
	{
	public:
		SplashScreenView(Models::SplashScreen*);
		~SplashScreenView();
		void Paint(HWND, PAINTSTRUCT*);
		void DrawSplashScreen(PAINTSTRUCT*, int, int);
	private:
		HDC _bitmapContext;
		HBITMAP _image;
		void DrawSplashScreen(PAINTSTRUCT*, int, int, std::vector<std::string>);
		Models::SplashScreen* _splashScreen;
	};
}
