#pragma once

#include "MenuController.h"
#include "GameController.h"
#include "PlayerController.h"
#include "DebugController.h"

namespace Controllers
{
	class GameEventHandler
	{
	public:
		GameEventHandler(GameController*);
		~GameEventHandler();
		void KeyDownEvent(WPARAM windowParameter, LPARAM messageParameter);
		void KeyUpEvent(WPARAM windowParameter, LPARAM messageParameter);
	private:
		GameController* _gameController;
	};
}