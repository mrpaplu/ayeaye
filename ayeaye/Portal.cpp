#include "stdafx.h"
#include "Portal.h"

using namespace Models;

Portal::Portal(void)
{
}


Portal::~Portal(void)
{
}

Portal* Portal::MakeGameObject(const Loaders::GameObjectProperties& props)
{
	Point<float> point = { props.posx, props.posy };
	Models::Portal * portal = new Models::Portal();
	Controllers::Animator * animator = new Controllers::PortalAnimator(props.sprites, portal);
	portal->SetWidth(props.width);
	portal->SetHeight(props.height);
	portal->SetPosX(props.posx);
	portal->SetPosY(props.posy);
	portal->SetType(props.type);
	portal->SetSprite(animator);
	return portal;
}