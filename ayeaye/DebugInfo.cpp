#include "stdafx.h"
#include "DebugInfo.h"

using namespace Models;

DebugInfo::DebugInfo()
{
	DebugOn = false;
}

DebugInfo::~DebugInfo()
{

}

void DebugInfo::SetFPS(int value)
{
	_FPS = value;
}

void DebugInfo::SetViewportPosition(Point<int> value)
{
	_viewportPosition = value;
}

int DebugInfo::GetFPS()
{
	return _FPS;
}

Point<int> DebugInfo::GetViewportPosition()
{
	return _viewportPosition;
}