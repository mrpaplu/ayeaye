#pragma once
#include "Enemy.h"
#include "MovingGameObject.h"

namespace Models
{
	class MovingEnemy : public MovingGameObject
	{
	public:
		MovingEnemy(void);
		~MovingEnemy(void);
		MovingEnemy(Point<float>);
		bool CanMoveLeft();
		bool CanMoveRight();
	private:
		Point<float> _startPosition;
		int _range;
	};
}
