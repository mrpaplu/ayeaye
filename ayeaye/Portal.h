#pragma once

#include "GameObject.h"
#include "PortalAnimator.h"
namespace Models
{
	class Portal :
		public GameObject
	{
	public:
		Portal(void);
		~Portal(void);
		static Portal* Portal::MakeGameObject(const Loaders::GameObjectProperties& props);
	};
}
