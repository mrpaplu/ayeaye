#pragma once

#include <vector>

namespace Models
{
	class SplashScreen
	{
	public:
		SplashScreen();
		~SplashScreen();
		bool SplashScreen::IsActive();
		void SplashScreen::SetActive(bool);
		std::vector<std::string> GetLines();
		void GetLevelSplash(std::string);
		void SplashScreenDefault();
		void SplashScreen::SplashScreenTextStarted();
		void SplashScreen::SplashScreenTextEnded();
		void SplashScreen::SplashScreenBeach();
		void SplashScreen::SplashScreenJungle();
		void SplashScreen::SplashScreenTown();
		void SplashScreen::SplashScreenOcean();
		void SplashScreen::SplashScreenShip();
		void SplashScreen::SplashScreenSpace();
		void SplashScreen::Credits();
		void SplashScreen::Lose();
	private:
		bool _isActive;
		std::vector<std::string> _lines;
	};
}