#include "stdafx.h"
#include "SoundController.h"

using namespace Controllers;

SoundController::SoundController(HWND windowHandler)
{
	InitializeDirectSound(windowHandler);
}

SoundController::SoundController()
{
}

SoundController::~SoundController(void)
{
	ShutdownDirectSound();
}

Views::SoundView* SoundController::GetView()
{
	return &_soundView;
}

bool SoundController::InitializeDirectSound(HWND hwnd)
{
	HRESULT result;
	DSBUFFERDESC bufferDesc;
	WAVEFORMATEX waveFormat;
 
	// Initialize the direct sound interface pointer for the default sound device.
	result = DirectSoundCreate8(NULL, &_directSound, NULL);
	if(FAILED(result))
	{
		return false;
	}
 
	// Set the cooperative level to priority so the format of the primary sound buffer can be modified.
	result = _directSound->SetCooperativeLevel(hwnd, DSSCL_PRIORITY);
	if(FAILED(result))
	{
		return false;
	}

	// Setup the primary buffer description.
	bufferDesc.dwSize = sizeof(DSBUFFERDESC);
	bufferDesc.dwFlags = DSBCAPS_PRIMARYBUFFER | DSBCAPS_CTRLVOLUME;
	bufferDesc.dwBufferBytes = 0;
	bufferDesc.dwReserved = 0;
	bufferDesc.lpwfxFormat = NULL;
	bufferDesc.guid3DAlgorithm = GUID_NULL;
 
	// Get control of the primary sound buffer on the default sound device.
	result = _directSound->CreateSoundBuffer(&bufferDesc, &_primaryBuffer, NULL);
	if(FAILED(result))
	{
		return false;
	}

	// Setup the format of the primary sound bufffer.
	// In this case it is a .WAV file recorded at 44,100 samples per second in 16-bit stereo (cd audio format).
	waveFormat.wFormatTag = WAVE_FORMAT_PCM;
	waveFormat.nSamplesPerSec = 44100;
	waveFormat.wBitsPerSample = 16;
	waveFormat.nChannels = 1;
	waveFormat.nBlockAlign = (waveFormat.wBitsPerSample / 8) * waveFormat.nChannels;
	waveFormat.nAvgBytesPerSec = waveFormat.nSamplesPerSec * waveFormat.nBlockAlign;
	waveFormat.cbSize = 0;
 
	// Set the primary buffer to be the wave format specified.
	result = _primaryBuffer->SetFormat(&waveFormat);
	if(FAILED(result))
	{
		return false;
	}
 
	return true;
}

void SoundController::ShutdownDirectSound()
{
	// Release the primary sound buffer pointer.
	if(_primaryBuffer)
	{
		_primaryBuffer->Release();
		_primaryBuffer = 0;
	}
 
	// Release the direct sound interface pointer.
	if(_directSound)
	{
		_directSound->Release();
		_directSound = 0;
	}
 
	return;
}

IDirectSound8* SoundController::GetDirectSound()
{
	return _directSound;
}

IDirectSoundBuffer* SoundController::GetPrimaryBuffer()
{
	return _primaryBuffer;
}