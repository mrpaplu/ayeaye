#pragma once
#include "collectable.h"
#include "RumAnimator.h"
namespace Models
{
	class Rum :
		public Collectable
	{
	public:
		Rum(void);
		~Rum(void);
		static Rum* Rum::MakeGameObject(const Loaders::GameObjectProperties& props);
	};
}