#include "stdafx.h"
#include "SplashScreenView.h"
#include <string>

using namespace Views;

SplashScreenView::SplashScreenView(Models::SplashScreen* splash)
{
	_image = (HBITMAP)LoadImage(NULL,
		"..\\GameData\\splashscreen_bg.bmp",
		IMAGE_BITMAP,
		0, 0,
		LR_LOADFROMFILE);
	_splashScreen = splash;
}


SplashScreenView::~SplashScreenView()
{
}

void SplashScreenView::Paint(HWND hWnd, PAINTSTRUCT* paintstruct)
{
	RECT clientSize;
	GetClientRect(hWnd, &clientSize);

	int centerX = clientSize.right / 2;
	int centerY = clientSize.bottom / 2;

	DrawSplashScreen(paintstruct, centerX, centerY);
}

void SplashScreenView::DrawSplashScreen(PAINTSTRUCT* paintstruct, int centerX, int centerY)
{
	HBRUSH brush = CreateSolidBrush(RGB(0, 0, 0));
	RECT rect = paintstruct->rcPaint;
	FillRect(paintstruct->hdc, &rect, brush);
	DeleteObject(brush);

	int fontHeight = 32;
	AddFontResource("..\\GameData\\PiratesBay.ttf");
	HFONT font = CreateFont(fontHeight, 0, 0, 0, 0, FALSE, FALSE, FALSE,
		DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY, FF_DONTCARE, TEXT("PiratesBay"));
	SetBkMode(paintstruct->hdc, TRANSPARENT);
	SetTextColor(paintstruct->hdc, RGB(28, 17, 11));
	SelectObject(paintstruct->hdc, font);
	UINT previousTextAlign = SetTextAlign(paintstruct->hdc, TA_CENTER);

	_bitmapContext = CreateCompatibleDC(paintstruct->hdc);
	SelectObject(_bitmapContext, _image);

	GdiTransparentBlt(paintstruct->hdc,
		centerX - 350,
		centerY - 180,
		700,
		320,
		_bitmapContext,
		paintstruct->rcPaint.top,
		paintstruct->rcPaint.left,
		700,
		320,
		RGB(255, 0, 255));

	int y = 0;
	for(int i = 0; i < _splashScreen->GetLines().size(); i++)
	{
		std::string line = _splashScreen->GetLines().at(i);
		TextOut(paintstruct->hdc, centerX, centerY-100 + y, line.c_str(), line.length());
		y += 32;
	}

	SetTextAlign(paintstruct->hdc, previousTextAlign);
	DeleteDC(_bitmapContext);
	RemoveFontResource("..\\GameData\\PiratesBay.ttf");
	DeleteObject(font);
}
