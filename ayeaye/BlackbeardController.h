#pragma once
#include "GameObjectController.h"

namespace Models
{
	class Blackbeard;
}

namespace Controllers
{
	class BlackbeardController : public GameObjectController
	{
	public:
		BlackbeardController(void);
		~BlackbeardController(void);
		Models::Blackbeard* GetModel();
		void Update(float);
		virtual void OnCollision(Models::GameObject*, Models::CollisionDirection);
		virtual  void OnOutOfLevel(Models::CollisionDirection dir, bool outOfLevel, RECT levelBounds);
		void FallDown(float);
		void SetModel(Models::Blackbeard*);
	private:
		Models::Blackbeard* _model;
	};
}