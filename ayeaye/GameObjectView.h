#pragma once

#include "GameObject.h"
#include "ViewportController.h"

namespace Views
{
	class GameObjectView
	{
	public:
		GameObjectView();
		~GameObjectView();
		void Paint(PAINTSTRUCT*, Controllers::ViewportController&, const Models::GameObject*);
	private:
		HDC _bitmapContext;
	};
}