#include "stdafx.h"
#include "Sound.h"

using namespace Models;

Sound::Sound()
{
	_secondaryBuffer = new IDirectSoundBuffer8*;
	_isPlaying = false;
	_exists = false;
}

void Sound::SetExists(bool ex)
{
	_exists = ex;
}

bool Sound::Exists()
{
	return _exists;
}
 
Sound::~Sound()
{
	if(_exists)
	{
		Shutdown();
		delete _secondaryBuffer;
	}
}

IDirectSoundBuffer8** Sound::GetSecondaryBuffer()
{
	return _secondaryBuffer;
}

void Sound::Shutdown()
{
	// Release the secondary sound buffer.
	if(*_secondaryBuffer)
	{
		(*_secondaryBuffer)->Release();
		*_secondaryBuffer = 0;
	}
	return;
}

void Sound::SetIsPlaying(bool p)
{
	_isPlaying = p;
}

bool Sound::IsPlaying()
{
	return _isPlaying;
}

