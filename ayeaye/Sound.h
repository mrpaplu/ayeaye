#pragma once

#include <mmsystem.h>
#include <dsound.h>
#include <stdio.h>

#pragma comment(lib, "dsound.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "winmm.lib")

namespace Models
{
	class Sound
	{
		private:
			IDirectSoundBuffer8** _secondaryBuffer;
			bool _isPlaying;
			bool _exists;
		public:
			IDirectSoundBuffer8** GetSecondaryBuffer();
			Sound(void);
			~Sound(void);
			void Shutdown();
			void SetIsPlaying(bool);
			bool IsPlaying();
			void SetExists(bool);
			bool Exists();
	};
}
