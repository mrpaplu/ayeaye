#pragma once

#include "Level.h"
#include "GameObjectView.h"
#include "DebugInfoView.h"
#include "BackgroundView.h"
#include "PlayerController.h"
#include "DebugController.h"
#include "GameObject.h"
#include "ViewportController.h"
#include "CollisionController.h"
#include "MenuController.h"
#include "MenuView.h"
#include "Game.h"
#include "GameState.h"
#include "SplashScreenController.h"
#include "ScoreController.h"
#include "SoundController.h"

namespace Controllers
{
	class GameController
	{
	public:
		~GameController();
		GameController(void);
		GameController(HWND);
		GameController(Models::Level, HWND, Point<int>);
		void Paint(PAINTSTRUCT*);
		void SetLastFrameTimeToCurrentTime();
		void CheckGameStates();
		void GameLoop();
		PlayerController* GetPlayerController();
		SoundController* GetSoundController();
		DebugController* GetDebugController();
		MenuController* GetMenuController();
		ViewportController* GetViewportController();
		SplashScreenController* GameController::GetSplashScreenController();
		void UpdateCollisionBox(Models::GameObject* gameObject);
		Models::Game* GetGame();
		void InitializeCollisionBoxes();
		void LoadLevel(std::string, int);
		void QuitLevel();
		void SaveGame();
		std::string* GetSavedGame();
		std::vector<std::string> GetLevelNames();
		bool HasNextLevel();
		void SetCurrentLevel(int);
		void PlayLevelMusic();
		void StopLevelMusic();
	private:
		Models::Level _level;
		ViewportController _viewportController;
		Models::Game* _game;
		Views::GameObjectView _gameObjectView;
		Views::DebugInfoView _debugInfoView;
		Views::BackgroundView _backgroundView;
		DebugController _debugController;
		MenuController _menuController;
		CollisionController _collisionController;
		ScoreController _scoreController;
		SoundController _soundController;
		HWND _window;
		int _framePerSecond;
		int _maxSleep;
		DWORD _nextGameTick;
		Models::DebugInfo* _debugInfoModel;
		int _timePassedMoving;
		int _timePassedAnimation;
		long _lastFrameTime;
		SplashScreenController _splashScreenController;
		std::vector<std::string> _levels;
		int _currentLevel;
	};
}