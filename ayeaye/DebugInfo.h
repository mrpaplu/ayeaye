#pragma once
#include "Point.h"

namespace Models {
	class DebugInfo
	{
	public:
		DebugInfo();
		~DebugInfo();
		bool DebugOn;
		void SetFPS(int);
		void SetViewportPosition(Point<int>);
		int GetFPS();
		Point<int> GetViewportPosition();
	private:
		int _FPS;
		Point<int> _viewportPosition;
	};
}