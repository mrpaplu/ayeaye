#include "stdafx.h"
#include <iostream>
#include <windows.h>
#include <iterator>
#include <vector>	 
#include "Sprite.h"
#include "Bitmap.h"

using namespace Models;

Sprite::Sprite()
{

}

Sprite::Sprite(RECT region, Bitmap* bitmap, const std::string name)
{
	_width = region.right - region.left;
	_height = region.bottom - region.top;
	_bitmap = bitmap;
	_region = region;
	_name = name;
}


Sprite::~Sprite()
{
}

void Sprite::Render(HDC hdc, int xDest, int yDest) const
{
	_bitmap->Render(hdc, _region.left, _region.top, xDest, yDest, _width, _height);
}

int Sprite::GetWidth() const
{
	return _width;
}

int Sprite::GetHeight() const 
{
	return _height;
}

const std::string& Sprite::GetName() const
{
	return _name;
}