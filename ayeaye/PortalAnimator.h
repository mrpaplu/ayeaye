#pragma once

#include "Animator.h"

namespace Controllers
{
	class PortalAnimator : public Animator
	{
	public:
		PortalAnimator(std::vector<Models::Sprite*>, Models::GameObject*);
		~PortalAnimator(void);
		void UpdateSprite(float delta);
	private:
		int _curOffset;
		float _timeout;
	};
}