#pragma once

#include "collectable.h"
#include "RumAnimator.h"
namespace Models
{
	class Vodka :
		public Collectable
	{
	public:
		Vodka();
		~Vodka();
		static Vodka* Vodka::MakeGameObject(const Loaders::GameObjectProperties& props);
	};
}