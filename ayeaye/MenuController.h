#pragma once

#include "Menu.h"
#include "MenuView.h"
#include "IngameMenu.h"
#include "SoundController.h"

namespace Controllers
{
	class GameController;
	class MenuController
	{
	public:
		MenuController(void);
		~MenuController(void);
		void SetHighscoreMenu();
		void SetMainMenu();
		void SetIngameMenu();
		void ToggleMenu();
		void SetActive(bool);
		Models::Menu* GetMenu();
		void SelectCurrentItem(GameController*);
		void Paint(HWND, PAINTSTRUCT*);
		void SetSoundController(SoundController*);
		Models::Sound* GetMusic();
		void MenuController::PlayMusic();
		void MenuController::StopMusic();
	private:
		MenuController(MenuController&);
		void operator=(MenuController&);
		MenuController(MenuController&&);
		void operator=(MenuController&&);
		void Cleanup();
		Models::Menu* _menu;
		Views::MenuView* _menuView;
		SoundController* _soundController;
		Models::Sound _menuMusic;
	};
}