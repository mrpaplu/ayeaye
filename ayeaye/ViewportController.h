#pragma once
#include "Viewport.h"

namespace Controllers
{
	class ViewportController
	{
	public:
		ViewportController();
		ViewportController(Point<int>, Point<int>);
		ViewportController(Point<int>);
		Models::Viewport* GetViewport();
		void Update(Point<float> playerPosition, int, int);
		Point<int> TranslateCoordinates(Point<int>);
		Point<float> TranslateCoordinates(Point<float>);
		bool IsVisible(const Point<float>, int, int);
		int* TranslateBackground(int, Point<int>&, int&, int, int);
		void UpdateWindowSize(Point<int>);
		int Top();
		int Bottom();
		int Left();
		int Right();
		~ViewportController(void);
	private:
		Models::Viewport _viewport;
	};
}