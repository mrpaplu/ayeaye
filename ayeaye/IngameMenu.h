#pragma once

#include "Menu.h"

namespace Models
{
	class IngameMenu : public Menu
	{
	public:
		IngameMenu(void);
		~IngameMenu(void);
		void SetMenuItems()
		{
			_items.push_back("Resume");
			_items.push_back("Save game");
			_items.push_back("Main menu");
			_items.push_back("Quit");
		}
	};
}