#include "stdafx.h"
#include "GameObjectView.h"
#include "Sprite.h"

#include <iostream>

using namespace Views;
GameObjectView::GameObjectView()
{
}

GameObjectView::~GameObjectView()
{
}

void GameObjectView::Paint(PAINTSTRUCT* ps, Controllers::ViewportController& viewportController, const Models::GameObject* gameObject)
{	
	Models::Sprite* sprite = gameObject->GetSprite();
	if (sprite == NULL)
		return;

	Point<float> position = gameObject->GetPosition();
	int width = sprite->GetWidth();
	int height = sprite->GetHeight();
	if (viewportController.IsVisible(position, width, height))
	{
		Point<float> translated = viewportController.TranslateCoordinates(position);
		sprite->Render(ps->hdc, translated.x, translated.y);

	}
}