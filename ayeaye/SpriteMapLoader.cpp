#include "stdafx.h"
#include "SpriteMapLoader.h"
#include "Utils.h"
#include "Sprite.h"
#include "LevelLoaderError.h"
#include <Windows.h>

using namespace Loaders;

std::vector<Models::Sprite*> SpriteMapLoader::LoadSpriteMap(std::istream& is, Models::Bitmap* bmp)
{
	RECT region;
	region.top = region.bottom = region.left = region.right = 0;
	std::string line;
	std::string name;
	std::vector<Models::Sprite*> sprites;
	while (Utils::getline(is, line))
	{
		if (Utils::Trim(line) == "")
			continue;

		std::string key = Utils::Trim(line.substr(0, line.find(':')));
		std::string value = Utils::Trim(line.substr(line.find(':') + 1));
		if (key == "END" && value == "SPRITEMAP")
			return sprites;
		else if (key == "BEGIN" && value == "SPRITE"){}//ignore
		else if (key == "TOP")
			region.top = atoi(value.c_str());
		else if (key == "BOTTOM")
			region.bottom = atoi(value.c_str());
		else if (key == "LEFT")
			region.left = atoi(value.c_str());
		else if (key == "RIGHT")
			region.right = atoi(value.c_str());
		else if (key == "TYPENAME")
			name = value;
		else if (key == "END" && value == "SPRITE")
			sprites.push_back(new Models::Sprite(region, bmp, name));
		else
			throw LevelLoad_error();
	}
	throw LevelLoad_error();
}