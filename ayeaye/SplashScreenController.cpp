#include "stdafx.h"
#include "SplashScreenController.h"

using namespace Controllers;

SplashScreenController::SplashScreenController()
{
	_splashScreen = new Models::SplashScreen();
	_splashScreenView = new Views::SplashScreenView(_splashScreen);
}


SplashScreenController::~SplashScreenController()
{
}

void SplashScreenController::Paint(HWND window, PAINTSTRUCT* ps)
{
	if (_splashScreen->IsActive())
	{
		_splashScreenView->Paint(window, ps);
	}
}

void SplashScreenController::TurnSplashScreenOff()
{
	_splashScreen->SetActive(false);
}

void SplashScreenController::TurnSplashScreenOn()
{
	_splashScreen->SetActive(true);
}

Models::SplashScreen* SplashScreenController::GetSplashScreen()
{
	return _splashScreen;
}