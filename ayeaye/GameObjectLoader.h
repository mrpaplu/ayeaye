#pragma once

#include <istream>
#include "GameObject.h"

namespace Loaders {
	class GameObjectLoader
	{
	public:
		GameObjectLoader();
		~GameObjectLoader();
		Models::GameObject* Load(HWND, std::istream&, const std::vector<Models::Sprite*>& lvlSpriteMap);
		void SetFromType(HWND, 
			GameObjectProperties&,
			const std::string& value,
			const std::vector<Models::Sprite*>& lvlSpriteMap);
	};
}