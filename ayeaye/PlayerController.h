#pragma once

#include "GameObjectController.h"
#include "SoundController.h"

namespace Models
{
	class Player;
}

namespace Controllers
{
	class PlayerController : public GameObjectController
	{
	public:
		~PlayerController();
		void SetPlayer(Models::Player*);
		void SetSoundController(SoundController*);
		Models::Player* GetPlayer();
		virtual void Update(float delta);
		virtual void OnCollision(Models::GameObject* other, Models::CollisionDirection direction); 
		virtual void OnOutOfLevel(Models::CollisionDirection dir, bool outOfLevel, RECT levelBounds);
		void InputLeft();
		void MoveLeft();
		void InputRight();
		void MoveRight();
		void InputTop();
		void InputBottom();
		void StopLeft();
		void StopRight();
		void StopY();
		void FallDown(float delta);
		void Jump();
		Models::Sound* GetRumSound();
		Models::Sound* GetHeartSound();
	private:
		Models::Player* _player;
		SoundController* _soundController;
		Models::Sound _rumSound;	
		Models::Sound _heartSound;	
	};
}