#pragma once

#include <vector>

namespace Models
{
	class Sprite;
	class Bitmap;
}

namespace Loaders
{
	class SpriteMapLoader
	{
	public:
		static std::vector<Models::Sprite*> LoadSpriteMap(std::istream& is, Models::Bitmap* bmp);
	};
}