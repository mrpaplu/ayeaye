#include "stdafx.h"
#include "CollisionController.h"
#include <vector>
#include <utility>
#include <iostream>

using namespace Controllers;

CollisionController::CollisionController()
{
}


CollisionController::~CollisionController()
{
}

CollisionController::CollisionController(Models::Level* level)
{
	_level = level;
}

static bool DoesCollideSimple(Models::GameObject* moveable, Models::GameObject* other)
{
	if (moveable->GetPosX() + moveable->GetWidth() < other->GetPosX())
		return false;
	if (moveable->GetPosX() > other->GetPosX() + other->GetWidth())
		return false;
	if (moveable->GetPosY() + moveable->GetHeight() < other->GetPosY())
		return false;
	if (moveable->GetPosY() > other->GetPosY() + other->GetHeight())
		return false;
	return true;
}

static bool DoesCollide(Models::GameObject* moveable, Models::GameObject* other)
{
	float boxCoef = 0.95f;
	float otherHalfWidth = other->GetWidth() / 2.f;
	float otherCenterX = other->GetPosX() + otherHalfWidth;
	float moveableRight = moveable->GetPosX() + (float)moveable->GetWidth();
	float otherBoundLeft = otherCenterX - otherHalfWidth * boxCoef;
	if (moveableRight < otherBoundLeft)
		return false;

	float otherBoundRight = otherCenterX + otherHalfWidth * boxCoef;
	if (moveable->GetPosX() > otherBoundRight)
		return false;

	float otherHalfHeight = other->GetHeight() / 2.f;
	float otherCenterY = other->GetPosY() + otherHalfHeight;
	float moveableBottom = moveable->GetPosY() + (float)moveable->GetHeight();
	float otherBoundTop = otherCenterY - otherHalfHeight * boxCoef;
	if (moveableBottom < otherBoundTop)
		return false;

	float otherBoundBottom = otherCenterY + otherHalfHeight * boxCoef;
	if (moveable->GetPosY() > otherBoundBottom)
		return false;		   
	return true;
}

static Point<float>	ReverseVector(Point<float> v)
{
	v.x = -v.x;
	v.y = -v.y;
	return v;
}

static Models::CollisionDirection ReverseDirection(Models::CollisionDirection d)
{
	switch (d)
	{
	case Models::CollisionDirection::Top:
		return Models::CollisionDirection::Bottom;
	case Models::CollisionDirection::Bottom: 
		return Models::CollisionDirection::Top;
	case Models::CollisionDirection::Left:	  
		return Models::CollisionDirection::Right;
	case Models::CollisionDirection::Right:		
		return Models::CollisionDirection::Left;
	default:
		return d;
	}
}

static Models::CollisionDirection TouchLevelBoundsOrOutOfLevel(Models::Level* level, Models::GameObject* g)
{
	if (g->GetPosX() < 0)
		return Models::CollisionDirection::Left;
	if (g->GetPosX() + g->GetWidth() > level->GetWidth())
		return Models::CollisionDirection::Right;
	if (g->GetPosY()  < 0)
		return Models::CollisionDirection::Top;
	if (g->GetPosY() + g->GetHeight() > level->GetHeight())
		return Models::CollisionDirection::Bottom;
	return Models::CollisionDirection::None;
}

static bool OutOfLevel(Models::Level* level, Models::GameObject* g)
{
	if (g->GetPosX() + g->GetWidth() < 0)
		return true;
	if (g->GetPosX() > level->GetWidth())
		return true;
	if (g->GetPosY() + g->GetHeight() < 0)
		return true;
	if (g->GetPosY() + g->GetHeight() > level->GetHeight())
		return true;
	return false;
}

void CollisionController::CheckCollisions(float delta)
{
	if (_level == NULL)
		return;
	Models::Level::gameobjects_iterator moveableIter;
	Models::Level::gameobjects_iterator otherIter;
	RECT levelBounds;
	levelBounds.top = levelBounds.left = 0;
	levelBounds.bottom = _level->GetHeight();
	levelBounds.right = _level->GetWidth();
	for (moveableIter = _level->GameObjectsBegin();
		moveableIter != _level->GameObjectsEnd();
		moveableIter++)
	{
		if (!(*moveableIter)->IsMoveable())
			continue;

		Models::CollisionDirection levelCol = TouchLevelBoundsOrOutOfLevel(_level, *moveableIter);
		if (levelCol != Models::CollisionDirection::None)
		{
			bool outOfLevel = OutOfLevel(_level, *moveableIter);
			if ((*moveableIter)->GetController())
				(*moveableIter)->GetController()->OnOutOfLevel(levelCol, outOfLevel, levelBounds);
		}


		Models::GameObject *moveable = *moveableIter;

		std::vector<std::pair<Models::GameObject*, Models::CollisionDirection>> collisions;

		for (otherIter = _level->GameObjectsBegin();
			otherIter != _level->GameObjectsEnd();
			otherIter++)
		{
			if (moveableIter == otherIter)
				continue;

			if (DoesCollide(*moveableIter, *otherIter))
			{
				Models::GameObject *other = *otherIter;
				Models::CollisionDirection dir = Models::CollisionDirection::None;

				float leftInsertion = std::numeric_limits<float>::infinity();
				float rightInsertion = std::numeric_limits<float>::infinity();
				float topInsertion = std::numeric_limits<float>::infinity();
				float bottomInsertion = std::numeric_limits<float>::infinity();
				if (moveable->GetPosX() + (float)moveable->GetWidth() > other->GetPosX())
				{
					leftInsertion = (moveable->GetPosX() + (float)moveable->GetWidth()) - other->GetPosX();
				}
				if (moveable->GetPosX() < other->GetPosX() + (float)other->GetWidth())
				{
					rightInsertion = (other->GetPosX() + (float)other->GetWidth()) - moveable->GetPosX();
				}
				if (moveable->GetPosY() + (float)moveable->GetHeight() > other->GetPosY())
				{
					topInsertion = (moveable->GetPosY() + (float)moveable->GetHeight()) - other->GetPosY();
				}
				if (moveable->GetPosY() < other->GetPosY() + (float)other->GetHeight())
				{
					bottomInsertion = (other->GetPosY() + (float)other->GetHeight()) - moveable->GetPosY();
				}
				if (topInsertion < rightInsertion
					&& topInsertion < leftInsertion
					&& topInsertion < bottomInsertion)
				{
					dir = Models::CollisionDirection::Top;
				}
				else if (bottomInsertion < rightInsertion
					&& bottomInsertion < topInsertion
					&& bottomInsertion < leftInsertion)
				{
					dir = Models::CollisionDirection::Bottom;
				}
				else if (leftInsertion < rightInsertion
					&& leftInsertion < topInsertion
					&& leftInsertion < bottomInsertion)
				{
					dir = Models::CollisionDirection::Left;
				}
				else if (rightInsertion < leftInsertion
					&& rightInsertion < topInsertion
					&& rightInsertion < bottomInsertion)
				{
					dir = Models::CollisionDirection::Right;
				}
				std::pair<Models::GameObject*, Models::CollisionDirection> c;
				c.first = other;
				c.second = dir;
				collisions.push_back(c);
			}
		}

		std::vector<std::pair<Models::GameObject*, Models::CollisionDirection>>::iterator it;
		for (it = collisions.begin(); it != collisions.end(); it++)
		{
			std::pair<Models::GameObject*, Models::CollisionDirection> collision = *it;
			if (collision.second == Models::CollisionDirection::Top
				|| collision.second == Models::CollisionDirection::Bottom)
			{  
				if (moveable->GetController())
					moveable->GetController()->OnCollision(collision.first, collision.second);
			}
		}
		for (it = collisions.begin(); it != collisions.end(); it++)
		{
			std::pair<Models::GameObject*, Models::CollisionDirection> collision = *it;
			if (collision.second == Models::CollisionDirection::Left
				|| collision.second == Models::CollisionDirection::Right)
			{
				if (DoesCollide(moveable, collision.first))
					if (moveable->GetController())
						moveable->GetController()->OnCollision(collision.first, collision.second);
			}
		}
	}
}