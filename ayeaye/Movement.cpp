#include "stdafx.h"
#include "Movement.h"

using namespace Models;

Movement::Movement(void)
{
	Point<float> p;
	p.x = 0;
	p.y = 0;
	_velocity = p;
}

void Movement::AlterMovement(Point<float> &p)
{
	_velocity.x += p.x;
	_velocity.y += p.y;
}

void Movement::Stop()
{
	_velocity.x = 0;
	_velocity.y = 0;
}

void Movement::StopX()
{
	_velocity.x = 0;
}

void Movement::StopY()
{
	_velocity.y = 0;
}

Point<float> Movement::GetVelocity()
{
	return _velocity;
}

void Movement::SetVelocity(Point<float> v)
{
	_velocity = v;
}

Movement::~Movement(void)
{
}


