﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AyeEdit.Models;

namespace AyeEdit.Controllers
{
    public class LevelController
    {
        private static Level Level { get; set; }

        public static Level LoadLevel(string path){
            string fileName = Path.GetFileNameWithoutExtension(path);
            using (var zipStream = new FileStream(path, FileMode.Open))
            {
                using (var zip = new ZipArchive(zipStream, ZipArchiveMode.Read))
                {
                    foreach (ZipArchiveEntry entry in zip.Entries)
                    {
                        // Extract information
                        if (Path.GetFileNameWithoutExtension(entry.Name) == fileName)
                        {
                            using (var stream = entry.Open())
                            {
                                using (var reader = new StreamReader(stream))
                                {
                                    LoadLevelInfo(reader);
                                }
                            }
                        }
                        if (Path.GetFileName(entry.Name) == "spritemap.bmp")
                        {
                            using (var stream = entry.Open())
                            {
                                Level.SpriteMapImage = new Bitmap(stream);
                            }
                        }
                    }
                }
            }

            return Level;
        }

        public void Save(Level level)
        {
            var saveFileDialog1 = new SaveFileDialog
            {
                Filter = "Aye level|*.aye",
                Title = "Save an Aye level File"
            };
            saveFileDialog1.ShowDialog();

            if (!string.IsNullOrEmpty(saveFileDialog1.FileName))
            {
                var fs = saveFileDialog1.OpenFile();
                var fileName = saveFileDialog1.FileName;

                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(saveFileDialog1.FileName);

                using (var memoryStream = new MemoryStream())
                {
                    using (var archive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                    {
                        ZipArchiveEntry ayeFile = archive.CreateEntry(fileNameWithoutExtension + ".txt");
                        using (var entryStream = ayeFile.Open())
                        {
                            using (StreamWriter writer = new StreamWriter(entryStream))
                            {
                                writer.WriteLine(level.ToString());
                            }
                        }
                        ZipArchiveEntry bmpFile = archive.CreateEntry("spritemap.bmp");
                        using (var entryStream = bmpFile.Open())
                        {
                            // using a tmp Memory stream, Saving directly to the entryStream doesnt work 
                            using (var tmpStream = new MemoryStream())
                            {                                       
                                level.SpriteMapImage.Save(tmpStream, ImageFormat.Bmp);
                                tmpStream.Position = 0;
                                tmpStream.CopyTo(entryStream);
                            }   
                        }
                    }
                    memoryStream.Seek(0, SeekOrigin.Begin);
                    memoryStream.CopyTo(fs);
                }
                fs.Close();
            }
        }

        public static void LoadLevelInfo(StreamReader file)
        {
            Level current = new Level(0,0);
            int width = 0;
            int height = 0;
            string line;
            // First line not the beginning? return;
            if (file.ReadLine() != "BEGIN:LEVEL")
            {
                Level = null;
                return;
            }

            int currentLayer = -1;
            while ((line = file.ReadLine()) != null)
            {
                if (line == "BEGIN:SPRITEMAP")
                {
                    LoadSpriteMap(current,file);
                }              

                if (line == "END:LEVEL")
                {
                    break;
                }

                string[] parameters = line.Split(':');

                switch (parameters[0])
                {
                    case "WIDTH":
                        current.Width = Convert.ToInt32(parameters[1]) / 32;
                        break;
                    case "HEIGHT":
                        current.Height = Convert.ToInt32(parameters[1]) / 32;
                        break;
                    case "NAME":
                        current.Name = parameters[1];
                        break;
                    case "GRAVITY":
                        current.Gravity = Convert.ToInt32(parameters[1]);
                        break;
                    case "BEGIN":
                        if (parameters[1] == "LAYER")
                        {
                            currentLayer++;
                            if (current.Layers.Count >= currentLayer)
                            {
                                current.Layers[currentLayer] = new Layer(current.Layers[currentLayer].Name,
                                    current.Width,
                                    current.Height);
                            }
                        }
                        if (parameters[1] == "GAMEOBJECT")
                        {
                            GameObject go = GameObjectController.LoadGameObject(file);
                            current.Layers[currentLayer].Add(go);
                        }
                        break;
                    default:
                        break;
                }
            }

            file.Close();
            Level = current;
        }

        private static void LoadSpriteMap(Level l, StreamReader file)
        {
            string line;
            while ((line = file.ReadLine()) != null)
            {
                line = line.Trim();
                if (line == "END:SPRITEMAP")
                {
                    return;
                }
                if (line == "BEGIN:SPRITE")
                {
                    var kvp = LoadSpriteInfo(file);
                    l.Sprites.Add(kvp.Key, kvp.Value);
                }    
            }     
        }
        private static KeyValuePair<string,Rectangle> LoadSpriteInfo(StreamReader file)
        {
            string name = "";
            int top = 0, bottom = 0, left = 0, right = 0;
            string line;
            while ((line = file.ReadLine()) != null)
            {
                line = line.Trim();
                if (line == "END:SPRITE")
                {
                    var rect = new Rectangle(left, top, right-left, bottom-top);
                    return new KeyValuePair<string, Rectangle>(name, rect);
                }
                string[] parameters = line.Split(':');
                parameters[0] = parameters[0].Trim();
                parameters[1] = parameters[1].Trim();
                switch (parameters[0])
                {
                    case "TOP":
                        top = Convert.ToInt32(parameters[1]);
                        break;
                    case "LEFT":
                        left = Convert.ToInt32(parameters[1]);
                        break;
                    case "RIGHT":
                        right = Convert.ToInt32(parameters[1]);
                        break;
                    case "BOTTOM":
                        bottom = Convert.ToInt32(parameters[1]);
                        break;
                    case "TYPENAME":
                        name = parameters[1];
                        break;
                    default:
                        break;
                }
            }
            return new KeyValuePair<string, Rectangle>();
        }
    }
}
