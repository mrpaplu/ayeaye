﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AyeEdit.Models;

namespace AyeEdit.Controllers
{
    public class GameObjectController
    {
        private static GameObjectController instance;

        public List<GameObject> GameObjects { get; set; }
        private GameObject CurrentGameObject { get; set; }

        private GameObjectController()
        {
            GameObjects = new List<GameObject>();
            LoadGameObjects(Properties.Resources.GameDataPath);
        }

        public static GameObjectController GetInstance()
        {
            if (instance == null)
            {
                instance = new GameObjectController();
            }
            return instance;
        }

        private void LoadGameObjects(string path)
        {
            path += "\\" + "GameObjects";
            if (!Directory.Exists(path))
            {
                MessageBox.Show("Wrong path to GameData\\GameObjects!");
                return;
            }

            foreach (string file in Directory.GetFiles(path))
            {
                if (File.Exists(file))
                {
                    LoadSingleGameObject(file);
                }
            }
        }

        private void LoadSingleGameObject(string path)
        {
            if (Path.GetExtension(path) != ".arg")
            {
                return;
            }

            string fileName = Path.GetFileNameWithoutExtension(path);

            using (var zipStream = new FileStream(path, FileMode.Open))
            {
                using (var zip = new ZipArchive(zipStream, ZipArchiveMode.Read))
                {
                    CurrentGameObject = new GameObject();
                    foreach (ZipArchiveEntry entry in zip.Entries)
                    {
                        
                        // Extract GameObject
                        if (Path.GetFileNameWithoutExtension(entry.Name) == fileName)
                        {
                            using (var stream = entry.Open())
                            {
                                using (var reader = new StreamReader(stream))
                                {
                                    CurrentGameObject = LoadGameObjectInfo(reader);
                                    reader.Close();
                                }
                            }
                        }
                        
                        if (Path.GetFileName(entry.Name) == "spritemap.bmp")
                        {
                            using (var stream = entry.Open())
                            {
                                CurrentGameObject.Image = new Bitmap(stream);
                            }
                        }
                        
                    }
                    if(CurrentGameObject != null)
                        GameObjects.Add(CurrentGameObject);
                }
            }
        }

        public static GameObject LoadGameObjectInfo(StreamReader file)
        {
            GameObject current = new GameObject();
            string line;

                // First line not the beginning? return;
            if (file.ReadLine() != "BEGIN:GAMEOBJECT")
            {
                return null;
            }

            while ((line = file.ReadLine()) != null)
            {
                line = line.Trim();
                if (line == "END:GAMEOBJECT")
                {
                    break;
                }

                string[] parameters = line.Split(':');

                switch (parameters[0])
                {
                    case "WIDTH":
                        current.Width = Convert.ToInt32(parameters[1]);
                        break;
                    case "HEIGHT":
                        current.Height = Convert.ToInt32(parameters[1]);
                        break;
                    case "NAME":
                        current.Name = parameters[1];
                        break;
                    case "COLOR":
                        current.Color = Color.FromName(parameters[1]);
                        break;
                    case "TYPE":
                        current.Type = parameters[1];
                        break;
                    default:
                        break;
                }
            }
            return current;
        }

     
        public static GameObject LoadGameObject(StreamReader file)
        {
            GameObjectController gol = GameObjectController.GetInstance();
            GameObject go = new GameObject();
            string line;

            while ((line = file.ReadLine()) != null)
            {
                if (line == "END:GAMEOBJECT")
                {
                    break;
                }

                string[] parameters = line.Split(':');

                switch (parameters[0])
                {
                    case "TYPE":
                        GameObject type = gol.GameObjects.FirstOrDefault(
                            x => String.Equals(x.Type, parameters[1], 
                                 StringComparison.CurrentCultureIgnoreCase));
                        go.Type = type.Type;
                        go.Name = type.Name;
                        go.Height = type.Height;
                        go.Image = type.Image;
                        go.Width = type.Width;
                        break;
                    case "POSX":
                        go.X = Convert.ToInt32(parameters[1]);
                        break;
                    case "POSY":
                        go.Y = Convert.ToInt32(parameters[1]);
                        break;
                    default:
                        break;
                }
            }
            return go;
        }
    }
}
