﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.IO.Compression;
using AyeEdit.Models;

namespace AyeEdit.Views
{
    public partial class LevelControl : UserControl
    {
        private EditorState EditorState = EditorState.GetInstance();

        public LevelControl()
        {
            InitializeComponent();
            EditorState.CurrentLayer = EditorState.Level.Layers.FirstOrDefault();
        }

        public LevelControl(int width, int height)
        {
            InitializeComponent();
            EditorState.Level = new Level(width, height);
            EditorState.CurrentLayer = EditorState.Level.Layers.FirstOrDefault();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Brush b;

            foreach (Layer layer in EditorState.Level.Layers)
            {
                for (int i = 0; i < layer.Width; i++)
                {
                    for (int j = 0; j < layer.Height; j++)
                    {
                        GameObject go = layer.Grid[i, j];
                        if (go != null)
                        {
                            if (go.Image == null)
                            {
                                if (EditorState.Level.Sprites.ContainsKey(go.Type))
                                {
                                    Rectangle dstRect = new Rectangle(go.X, go.Y, go.Width, go.Height);
                                    Rectangle srcRect = EditorState.Level.Sprites[go.Type];
                                    g.DrawImage(EditorState.Level.SpriteMapImage, dstRect, srcRect, GraphicsUnit.Pixel);    
                                }
                                else
                                {
                                    b = new SolidBrush(go.Color);
                                    g.FillRectangle(b, go.X, go.Y, go.Width, go.Height);   
                                } 
                            }
                            else
                            {
                                Rectangle dstRect = new Rectangle(go.X, go.Y, go.Width, go.Height);
                                Rectangle srcRect = new Rectangle(0,0,go.Width, go.Height);
                                g.DrawImage(go.Image, dstRect, srcRect, GraphicsUnit.Pixel);
                            }
                        }
                    }
                }
            }
            for (int i = EditorState.GridSize; i <= this.Height; i += EditorState.GridSize)
            {
                g.DrawLine(Pens.Gray, 0, i, Width, i);
            }
            for (int i = EditorState.GridSize; i <= Width; i += EditorState.GridSize)
            {
                g.DrawLine(Pens.Gray, i, 0, i, Height);
            }
            base.OnPaint(e);
        }

        private void LevelControl_Load_1(object sender, EventArgs e)
        {
            Width = EditorState.GridSize * EditorState.Level.Width;
            Height = EditorState.GridSize * EditorState.Level.Height;
        }

        private void placeGameObject(object sender, MouseEventArgs e)
        {
            if (EditorState.CurrentGameObject != null)
            {
                if (e.Button == MouseButtons.Left)
                {
                    Point coords = PointToClient(Cursor.Position);

                    GameObject go = new GameObject
                    {
                        Width = EditorState.CurrentGameObject.Width,
                        Height = EditorState.CurrentGameObject.Height,
                        X = coords.X - coords.X % EditorState.GridSize,
                        Y = coords.Y - coords.Y % EditorState.GridSize,
                        Color = EditorState.CurrentGameObject.Color,
                        Type = EditorState.CurrentGameObject.Type,
                        Image = EditorState.CurrentGameObject.Image
                    };
                    EditorState.CurrentLayer.Add(go);
                    Invalidate();
                }
            }
            if (e.Button == MouseButtons.Right)
            {
                Point coords = PointToClient(Cursor.Position);

                EditorState.CurrentLayer.Remove(
                    coords.X - coords.X % EditorState.GridSize,
                    coords.Y - coords.Y % EditorState.GridSize
                );
                Invalidate();
            }
        }
    }
}
