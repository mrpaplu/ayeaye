﻿namespace AyeEdit.Views
{
    partial class SpriteMapEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.loadImgBtn = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.rightNum = new System.Windows.Forms.NumericUpDown();
            this.leftNum = new System.Windows.Forms.NumericUpDown();
            this.bottomNum = new System.Windows.Forms.NumericUpDown();
            this.topNum = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.inSpriteMapCheckBox = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rightNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bottomNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.topNum)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(233, 205);
            this.panel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // loadImgBtn
            // 
            this.loadImgBtn.Location = new System.Drawing.Point(152, 223);
            this.loadImgBtn.Name = "loadImgBtn";
            this.loadImgBtn.Size = new System.Drawing.Size(93, 23);
            this.loadImgBtn.TabIndex = 1;
            this.loadImgBtn.Text = "Load Image...";
            this.loadImgBtn.UseVisualStyleBackColor = true;
            this.loadImgBtn.Click += new System.EventHandler(this.loadImgBtn_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(251, 12);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(120, 355);
            this.listBox1.TabIndex = 2;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.inSpriteMapCheckBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.rightNum);
            this.groupBox1.Controls.Add(this.leftNum);
            this.groupBox1.Controls.Add(this.bottomNum);
            this.groupBox1.Controls.Add(this.topNum);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(378, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(223, 155);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sprite Info";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 125);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Is in SpriteMap";
            // 
            // rightNum
            // 
            this.rightNum.Location = new System.Drawing.Point(157, 98);
            this.rightNum.Name = "rightNum";
            this.rightNum.Size = new System.Drawing.Size(60, 20);
            this.rightNum.TabIndex = 8;
            this.rightNum.ValueChanged += new System.EventHandler(this.rightNum_ValueChanged);
            // 
            // leftNum
            // 
            this.leftNum.Location = new System.Drawing.Point(157, 72);
            this.leftNum.Name = "leftNum";
            this.leftNum.Size = new System.Drawing.Size(60, 20);
            this.leftNum.TabIndex = 7;
            this.leftNum.ValueChanged += new System.EventHandler(this.leftNum_ValueChanged);
            // 
            // bottomNum
            // 
            this.bottomNum.Location = new System.Drawing.Point(157, 46);
            this.bottomNum.Name = "bottomNum";
            this.bottomNum.Size = new System.Drawing.Size(60, 20);
            this.bottomNum.TabIndex = 6;
            this.bottomNum.ValueChanged += new System.EventHandler(this.bottomNum_ValueChanged);
            // 
            // topNum
            // 
            this.topNum.Location = new System.Drawing.Point(157, 20);
            this.topNum.Name = "topNum";
            this.topNum.Size = new System.Drawing.Size(60, 20);
            this.topNum.TabIndex = 5;
            this.topNum.ValueChanged += new System.EventHandler(this.topNum_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Right";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Left";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Bottom";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Top";
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(535, 375);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 4;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(454, 375);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 5;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // inSpriteMapCheckBox
            // 
            this.inSpriteMapCheckBox.AutoSize = true;
            this.inSpriteMapCheckBox.Location = new System.Drawing.Point(202, 125);
            this.inSpriteMapCheckBox.Name = "inSpriteMapCheckBox";
            this.inSpriteMapCheckBox.Size = new System.Drawing.Size(15, 14);
            this.inSpriteMapCheckBox.TabIndex = 10;
            this.inSpriteMapCheckBox.UseVisualStyleBackColor = true;
            this.inSpriteMapCheckBox.CheckedChanged += new System.EventHandler(this.inSpriteMapCheckBox_CheckedChanged);
            // 
            // SpriteMapEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 410);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.loadImgBtn);
            this.Controls.Add(this.panel1);
            this.Name = "SpriteMapEditForm";
            this.Text = "SpriteMap";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rightNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bottomNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.topNum)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button loadImgBtn;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown rightNum;
        private System.Windows.Forms.NumericUpDown leftNum;
        private System.Windows.Forms.NumericUpDown bottomNum;
        private System.Windows.Forms.NumericUpDown topNum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox inSpriteMapCheckBox;
    }
}