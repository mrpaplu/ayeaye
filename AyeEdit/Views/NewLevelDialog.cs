﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AyeEdit.Views
{
    public partial class NewLevelDialog : Form
    {
        public int LevelWidth { 
            get 
            {
                return (int) numericUpDown1.Value;
            } 
        }
        public int LevelHeight
        {
            get
            {
                return (int) numericUpDown2.Value;
            }
        }

        public NewLevelDialog()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Dispose();
        }
    }
}
