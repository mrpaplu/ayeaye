﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace AyeEdit.Views
{
    public partial class SpriteMapEditForm : Form
    {
        private Bitmap _spritesBitmap;
        private List<string> _possibleObjectTypes;
                                                         
        public Dictionary<string, Rectangle> Sprites { get; set; }

        public Bitmap SpritesBitmap
        {
            get { return _spritesBitmap; }
            set
            {
                _spritesBitmap = value;
                pictureBox1.BackgroundImage = _spritesBitmap;
                if(pictureBox1.BackgroundImage != null)
                {
                    pictureBox1.Size = new Size(_spritesBitmap.Width + 1, _spritesBitmap.Height + 1);
                    rightNum.Maximum = leftNum.Maximum = _spritesBitmap.Width;
                    topNum.Maximum = bottomNum.Maximum = _spritesBitmap.Height;
                }
                  
                pictureBox1.Invalidate();
            }
        }

        public List<string> PossibleObjectTypes
        {
            get { return _possibleObjectTypes; }
            set
            {
                _possibleObjectTypes = value;             
                listBox1.DataSource = _possibleObjectTypes;
                listBox1.SelectedIndex = 0;
                UpdateSpriteInfo();
            }
        }

        public SpriteMapEditForm()
        {
            InitializeComponent();
            Sprites = new Dictionary<string, Rectangle>();
            pictureBox1.Paint += PictureBox1OnPaint;
        }

        private void PictureBox1OnPaint(object sender, PaintEventArgs e)
        {
            foreach (var sprite in Sprites)
            {
                e.Graphics.DrawRectangle(Pens.Black, sprite.Value);    
            }
        }

        private void saveButton_Click(object sender, System.EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void cancelButton_Click(object sender, System.EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            UpdateSpriteInfo();
        }

        private void UpdateSpriteInfo()
        {
            if (listBox1.SelectedIndex < 0)
                return;
            var objectName = (string) listBox1.SelectedItem;
            if (Sprites.ContainsKey(objectName))
            {
                topNum.Value = Sprites[objectName].Top;
                bottomNum.Value = Sprites[objectName].Bottom;
                leftNum.Value = Sprites[objectName].Left;
                rightNum.Value = Sprites[objectName].Right;
                inSpriteMapCheckBox.Checked = true;
            }
            else
            {
                topNum.Value = 0;
                bottomNum.Value = 0;
                leftNum.Value = 0;
                rightNum.Value = 0;
                inSpriteMapCheckBox.Checked = false;
            }
            pictureBox1.Invalidate();
        }

        private void inSpriteMapCheckBox_CheckedChanged(object sender, System.EventArgs e)
        {
            if (inSpriteMapCheckBox.Checked)
            {
                var rect = new Rectangle((int) leftNum.Value,
                    (int) topNum.Value,
                    (int) (rightNum.Value - leftNum.Value),
                    (int) (bottomNum.Value - topNum.Value));
                Sprites[(string) listBox1.SelectedItem] = rect;
            }
            else
            {
                Sprites.Remove((string) listBox1.SelectedItem);
            }
            pictureBox1.Invalidate();
        }

        private void topNum_ValueChanged(object sender, System.EventArgs e)
        {
            string name = (string) listBox1.SelectedItem;
            if (!Sprites.ContainsKey(name))
                return;
            var rect = Sprites[name];                     
            rect.Height = rect.Bottom - (int)topNum.Value;
            rect.Y = (int) topNum.Value;
            Sprites[name] = rect;
            pictureBox1.Invalidate();
        }

        private void bottomNum_ValueChanged(object sender, System.EventArgs e)
        {
            string name = (string)listBox1.SelectedItem;
            if (!Sprites.ContainsKey(name))
                return;
            var rect = Sprites[name];
            rect.Height = (int)bottomNum.Value - rect.Y;
            Sprites[name] = rect;
            pictureBox1.Invalidate();
        }

        private void leftNum_ValueChanged(object sender, System.EventArgs e)
        {
            string name = (string)listBox1.SelectedItem;
            if (!Sprites.ContainsKey(name))
                return;
            var rect = Sprites[name];
            rect.Width = rect.Right - (int)leftNum.Value;
            rect.X = (int)leftNum.Value;
            Sprites[name] = rect;
            pictureBox1.Invalidate();
        }

        private void rightNum_ValueChanged(object sender, System.EventArgs e)
        {
            string name = (string)listBox1.SelectedItem;
            if (!Sprites.ContainsKey(name))
                return;
            var rect = Sprites[name];
            rect.Width = (int)rightNum.Value - rect.X;
            Sprites[name] = rect;
            pictureBox1.Invalidate();
        }

        private void loadImgBtn_Click(object sender, EventArgs e)
        {
            var openDialog = new OpenFileDialog();
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {   
                    SpritesBitmap = new Bitmap(openDialog.FileName);
                }
                catch (Exception)
                {
                    
                    throw;
                }
            }
        }
    }
}
