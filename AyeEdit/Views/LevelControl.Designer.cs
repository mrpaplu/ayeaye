﻿namespace AyeEdit.Views
{
    partial class LevelControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // LevelControl
            // 
            this.AllowDrop = true;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DoubleBuffered = true;
            this.Name = "LevelControl";
            this.Size = new System.Drawing.Size(504, 497);
            this.Load += new System.EventHandler(this.LevelControl_Load_1);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.placeGameObject);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.placeGameObject);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
