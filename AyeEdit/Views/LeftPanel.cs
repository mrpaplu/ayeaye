﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AyeEdit.Models;
using System.IO;
using AyeEdit.Controllers;

namespace AyeEdit.Views
{
    public partial class LeftPanel : UserControl
    {
        private EditorState EditorState = EditorState.GetInstance();
        GameObjectController gol;

        public LeftPanel()
        {
            InitializeComponent();
        }

        private void LoadLayers()
        {
            Level level = new Level(1, 1);
            var dataSource = new List<Layer>();
            foreach (Layer layer in level.Layers)
            {
                dataSource.Add(layer);
            }

            this.comboBox1.DataSource = dataSource;
            this.comboBox1.DisplayMember = "Name";
            this.comboBox1.ValueMember = "Name";

            this.comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void LoadGameObjects()
        {
            foreach (GameObject go in gol.GameObjects)
            {
                if(go != null)
                    listBox1.Items.Add(go);
            }
        }

        private void LeftPanel_Load(object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                gol = GameObjectController.GetInstance();
                LoadGameObjects();
                LoadLayers();
            }
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            if (EditorState.Level != null)
            {
                string layerName = (string) comboBox1.SelectedValue;
                EditorState.CurrentLayer = EditorState.Level.Layers.FirstOrDefault(x => x.Name == layerName);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex < 0)
                return;
            GameObject go = listBox1.Items[listBox1.SelectedIndex] as GameObject;
            EditorState.CurrentGameObject = go;
        }

        private void listBox1_DrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();
            GameObject go = listBox1.Items[e.Index] as GameObject;

            //GraphicsUnit units = GraphicsUnit.Pixel;
            //Rectangle imageRect = new Rectangle(e.Bounds.Left, e.Bounds.Top, go.Image.Width, go.Image.Height);
            Rectangle destRect = new Rectangle(e.Bounds.Left + 2, e.Bounds.Top + 2, e.Bounds.Height - 4, e.Bounds.Height - 4);
            if (go.Image != null)
            {
                Rectangle srcRect = new Rectangle(0,0,go.Width, go.Height);
                e.Graphics.DrawImage(go.Image, destRect, srcRect, GraphicsUnit.Pixel);    
            }
            else if(EditorState.Level != null && EditorState.Level.Sprites.ContainsKey(go.Type))
            {
                Rectangle srcRect = EditorState.Level.Sprites[go.Type];
                e.Graphics.DrawImage(EditorState.Level.SpriteMapImage, destRect, srcRect, GraphicsUnit.Pixel); 
            }
                

            Point stringPoint = new Point(e.Bounds.Left + e.Bounds.Height + 4, e.Bounds.Top + e.Bounds.Height / 4);
            e.Graphics.DrawString(go.Name, SystemFonts.DefaultFont, Brushes.Black, stringPoint);
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            if (EditorState.Level != null)
            {
                EditorState.Level.Gravity = (int)numericUpDown1.Value;
            }
        }

        public void SetGravity()
        {
            numericUpDown1.Value = EditorState.Level.Gravity;
        }
    }
}
