﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.IO.Compression;
using AyeEdit.Properties;
using System.Drawing;
using AyeEdit.Models;
using AyeEdit.Views;
using AyeEdit.Controllers;

namespace AyeEdit.Views
{
    public partial class AyeEditForm : Form
    {
        public LevelControl LevelControl { get; set; }
        public EditorState EditorState = EditorState.GetInstance();

        public AyeEditForm()
        {
            InitializeComponent();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewLevelDialog dialog = new NewLevelDialog();
            if (dialog.ShowDialog(this) == DialogResult.OK)
            {
                int width = dialog.LevelWidth;
                int height = dialog.LevelHeight;
                LevelControl = new LevelControl(width, height);
                preventScrollOnFocusPanel1.Controls.Clear();
                preventScrollOnFocusPanel1.Controls.Add(LevelControl);
                leftPanel1.SetGravity();
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (LevelControl == null)
            {
                MessageBox.Show("Create a level first.");
                return;
            }
            LevelController controller = new LevelController();
            controller.Save(EditorState.Level);
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                EditorState.Level = LevelController.LoadLevel(openFileDialog1.FileName);
                LevelControl = new LevelControl();
                preventScrollOnFocusPanel1.Controls.Clear();
                preventScrollOnFocusPanel1.Controls.Add(LevelControl);
                leftPanel1.SetGravity();
                leftPanel1.Invalidate(true);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void levelSpriteMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (EditorState.Level == null)
            {
                MessageBox.Show("Create a level first.");
                return;
            }          
            var gol = GameObjectController.GetInstance();
            var spriteMapEditDialog = new SpriteMapEditForm
            {
                SpritesBitmap = EditorState.Level.SpriteMapImage,     
                Sprites = new Dictionary<string, Rectangle>(EditorState.Level.Sprites),
                PossibleObjectTypes = gol.GameObjects.Select(x => x.Type).ToList() ,
            };
            if (spriteMapEditDialog.ShowDialog() == DialogResult.OK)
            {
                EditorState.Level.Sprites = spriteMapEditDialog.Sprites;
                EditorState.Level.SpriteMapImage = spriteMapEditDialog.SpritesBitmap;
            }
            Invalidate(true);
        }
    }
}
