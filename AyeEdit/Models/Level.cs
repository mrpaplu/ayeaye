﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AyeEdit.Models
{
    public class Level
    {
        public string Name { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public Color BackgroundColor { get; set; }
        public int Gravity { get; set; }

        public List<Layer> Layers { get; set; }
        public Bitmap SpriteMapImage { get; set; }

        public Dictionary<string, Rectangle> Sprites { get; set; } 

        public Level(int width, int height)
        {
            Width = width;
            Height = height;

            Layers = new List<Layer>();
            Layers.Add(new Layer("Solids", width, height));
            Sprites = new Dictionary<string, Rectangle>();
        }

        /// <summary>
        /// String of Level file format
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("BEGIN:LEVEL");
            sb.AppendLine("WIDTH:" + Width*32);
            sb.AppendLine("HEIGHT:" + Height*32);
            sb.AppendLine("GRAVITY:" + Gravity);

            sb.AppendLine("BEGIN:SPRITEMAP");
            foreach (var kvp in Sprites)
            {
                sb.AppendLine("BEGIN:SPRITE");
                sb.AppendLine("TYPENAME:" + kvp.Key);
                sb.AppendLine("TOP:" + kvp.Value.Top);
                sb.AppendLine("BOTTOM:" + kvp.Value.Bottom);
                sb.AppendLine("RIGHT:" + kvp.Value.Right);
                sb.AppendLine("LEFT:" + kvp.Value.Left);
                sb.AppendLine("END:SPRITE");
            }
            sb.AppendLine("END:SPRITEMAP");

            foreach (Layer layer in Layers)
            {
                sb.AppendLine(layer.ToString());
            }

            sb.AppendLine("END:LEVEL");

            return sb.ToString();
        }
    }
}
