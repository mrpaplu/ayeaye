﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AyeEdit.Models
{
    public class Layer
    {
        public string Name { get; set; }

        public GameObject[,] Grid { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        private EditorState EditorState = EditorState.GetInstance();

        public Layer(string name, int width, int height)
        {
            Name = name;
            Width = width;
            Height = height;
            Grid = new GameObject[width, height];
        }

        public void Add(GameObject go)
        {
            int gridX = go.X / EditorState.GridSize;
            int gridY = go.Y / EditorState.GridSize;
            if (gridX >= Width || gridY >= Height || gridX < 0 || gridY < 0)
            {
                return;
            }
            Grid[gridX, gridY] = go;
        }

        public void Remove(int x, int y)
        {
            int gridX = x / EditorState.GridSize;
            int gridY = y / EditorState.GridSize;
            if (gridX >= Width || gridY >= Height || gridX < 0 || gridY < 0)
            {
                return;
            }
            Grid[gridX, gridY] = null;
        }

        /// <summary>
        /// String of Level file format
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            GameObject go;

            sb.AppendLine("BEGIN:LAYER");
            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    go = Grid[i, j];
                    if (go != null)
                    {
                        sb.AppendLine(go.ToLevelString());
                    }
                }
            }
            sb.AppendLine("END:LAYER");

            return sb.ToString();
        }
    }
}
