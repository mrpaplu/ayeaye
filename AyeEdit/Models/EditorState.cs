﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AyeEdit.Models
{
    public class EditorState
    {
        private static EditorState Instance;

        public Layer CurrentLayer { get; set; }
        public int GridSize { get; set; }
        public Level Level { get; set; }
        public bool SnapToGrid { get; set; }
        public GameObject CurrentGameObject { get; set; }

        private EditorState() 
        {
            GridSize = 32;
            SnapToGrid = true;
        }

        public static EditorState GetInstance()
        {
            if(Instance == null)
            {
                Instance = new EditorState();
            }
            return Instance;
        }
    }
}
