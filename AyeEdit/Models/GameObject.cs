﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace AyeEdit.Models
{
    public class GameObject
    {
        public string Name { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public Color Color { get; set; }
        public string Type { get; set; }
        public Bitmap Image { get; set; }

        public override string ToString()
        {
            if (Name == null)
            {
                return String.Empty;
            }
            return Name;
        }

        /// <summary>
        /// String in the Level file format
        /// </summary>
        /// <returns></returns>
        public string ToLevelString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("BEGIN:GAMEOBJECT");

            sb.AppendLine("TYPE:" + Type);
            sb.AppendLine("POSX:" + X);
            sb.AppendLine("POSY:" + Y);

            sb.AppendLine("END:GAMEOBJECT");

            return sb.ToString();
        }

        /// <summary>
        /// String in the GameObject file format
        /// </summary>
        /// <returns></returns>
        public string ToGameObjectString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("BEGIN:GAMEOBJECT");

            sb.AppendLine("NAME:" + Name);
            sb.AppendLine("TYPE:" + Type);
            sb.AppendLine("WIDTH:" + Width);
            sb.AppendLine("HEIGHT:" + Height);
            sb.AppendLine("COLOR:" + Color);

            sb.AppendLine("END:GAMEOBJECT");

            return sb.ToString();
        }
    }
}
